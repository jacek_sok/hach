package edu.pk.hach.server.configuration

trait ConfigurationProvider {
    def getConfiguration: Configuration
}
