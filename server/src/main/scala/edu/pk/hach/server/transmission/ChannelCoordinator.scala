package edu.pk.hach.server.transmission

import scala.collection.mutable
import edu.pk.hach.server.session.SessionId
import akka.actor.{Terminated, Actor, ActorRef, ActorSystem}
import org.springframework.beans.factory.annotation.Autowired
import edu.pk.hach.server.spring.ActorBean
import org.slf4j.LoggerFactory

@ActorBean
class ChannelCoordinator @Autowired()(actors: ActorSystem) extends Actor {

    import ChannelCoordinator._

    private[transmission] val session2seed = new mutable.HashMap[SessionId, ActorRef]()
    private[transmission] val seed2session = new mutable.HashMap[ActorRef, SessionId]()

    override def receive = {
        case SeedConnected(id, producer) =>
            Log.debug("Creating new stream channel for seed: {}", producer)
            session2seed(id) = producer
            seed2session(producer) = id
            context.watch(producer)
            sender ! "OK"

        case LeechConnected(session, seedId, actor) =>
            val sessionId = SessionId(session.uuid, seedId)
            session2seed.get(sessionId) match {
                case None =>
                    Log.debug("No channel for session {}/{}", session.uuid, session.peer)
                    sender ! LeechFailure("No such channel: (%s, %s)".format(session, seedId))
                case Some(seed) =>
                    Log.debug("Leech '{}' connected to seed '{}'", actor, seed)
                    actor ! seed
                    sender ! LeechSuccess(seed)
            }

        case StatRequest =>
            sender ! StatResponse(seed2session.values.map(_.peer).toList)

        case Terminated(seed) =>
            Log.debug("Terminating channel for seed: {}", seed)
            seed2session.remove(seed) match {
                case Some(session) => session2seed.remove(session)
                case _ => Log.warn("Seed doesn't exist in coordinator table: {}", seed)
            }
    }
}

object ChannelCoordinator {
    val Log = LoggerFactory.getLogger(classOf[ChannelCoordinator])

    case class LeechConnected(session: SessionId, seed: String, actor: ActorRef)

    case class SeedConnected(session: SessionId, producer: ActorRef)

    case class LeechSuccess(seed: ActorRef)

    case class LeechFailure(reason: String)

    case object StatRequest

    case class StatResponse(sessions: List[String])
}