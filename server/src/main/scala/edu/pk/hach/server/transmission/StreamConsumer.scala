package edu.pk.hach.server.transmission

import akka.actor.{ActorRef, Actor}
import edu.pk.hach.server.transmission.StreamConsumer._
import edu.pk.hach.protocol.HttpConverters
import spray.http.{ChunkedMessageEnd, MessageChunk}
import akka.actor.Terminated
import org.slf4j.LoggerFactory
import java.util.concurrent.{TimeUnit, ArrayBlockingQueue}

class StreamConsumer(receiver: ActorRef) extends Actor with HttpConverters {
    private var stop = false
    private val queue = new ArrayBlockingQueue[MessageChunk](100)

    val thread = new Thread(new Runnable {
        override def run() {
            while (!stop) {
                val chunk = queue.poll(500, TimeUnit.MILLISECONDS)
                if (chunk != null) {
                    Log.trace("Message chunk: {}", chunk.data)
                    receiver ! chunk
                }
            }
        }
    })

    override def receive = {
        case seed: ActorRef =>
            Log.debug("Attached to seed: {}", seed)
            seed ! (self, queue)
            thread.start()
            context.watch(seed)
            context.watch(receiver)

        case Terminated(actor) =>
            if (actor.eq(receiver)) {
                Log.warn("Leech disconnected on the other end")
            } else {
                Log.debug("Seed disconnected, stopping leech: {}", self)
                receiver ! ChunkedMessageEnd()
            }
            stop = true
            context.stop(self)
    }
}

object StreamConsumer {
    val Log = LoggerFactory.getLogger(classOf[StreamConsumer])
}
