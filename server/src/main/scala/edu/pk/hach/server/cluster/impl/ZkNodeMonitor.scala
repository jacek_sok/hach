package edu.pk.hach.server.cluster.impl

import edu.pk.hach.server.configuration.ConfigurationProvider
import edu.pk.hach.server.transmission.ChannelCoordinator.StatRequest
import ZkNodeMonitor._
import scala.collection.JavaConversions._
import scala.concurrent.duration.DurationInt
import akka.actor.{ActorSystem, ActorRef}
import akka.pattern.ask
import org.apache.zookeeper.ZooDefs.Ids
import org.apache.zookeeper._
import org.slf4j.LoggerFactory
import org.apache.zookeeper.Watcher.Event.{KeeperState, EventType}
import akka.util.Timeout
import javax.annotation.PreDestroy
import java.util.concurrent.atomic.AtomicInteger
import java.nio.ByteBuffer
import edu.pk.hach.server.transmission.ChannelCoordinator.StatResponse
import scala.util.Failure
import scala.util.Success
import org.apache.zookeeper.KeeperException.Code._
import edu.pk.hach.server.cluster.NodeMonitor
import edu.pk.hach.server.spring.ImplicitActorSystem
import org.apache.zookeeper.KeeperException.NodeExistsException
import java.util.concurrent.ConcurrentHashMap
import org.apache.zookeeper.data.Stat

@ImplicitActorSystem
class ZkNodeMonitor(configurationProvider: ConfigurationProvider, channelCoordinator: ActorRef)
                   (implicit actorSystem: ActorSystem) extends NodeMonitor with Watcher {

    import actorSystem.dispatcher

    private val cfg = configurationProvider.getConfiguration.zooKeeper
    private val thisHost = configurationProvider.getConfiguration.hostId
    private val Log = LoggerFactory.getLogger(classOf[ZkNodeMonitor] + "[" + thisHost + "]")
    private implicit val ordering = new Ordering[(String, Int)] {
        def compare(x: (String, Int), y: (String, Int)): Int = {
            val rs = x._2.compareTo(y._2)
            if (rs == 0 && x._1 == thisHost) Integer.MIN_VALUE
            else if (rs == 0 && y._1 == thisHost) Integer.MAX_VALUE
            else rs
        }
    }
    private val localStats = new AtomicInteger()
    private val stats = new ConcurrentHashMap[String, Int]()
    private implicit val internalTimeout = Timeout((cfg.timeout / 2) milliseconds)
    private val zkClient = new ZooKeeper(cfg.hosts.mkString(","), cfg.timeout, this)
    private lazy val myPath =
        try zkClient.create(StatPath + "/" + thisHost, writeInt(localStats.get()), Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL)
        catch {
            case e: NodeExistsException => StatPath + "/" + thisHost
        }

    private def readStat(host: String) = readInt(zkClient.getData(StatPath + "/" + host, true, null))

    private def readInt(array: Array[Byte]) = ByteBuffer.wrap(array).getInt

    private def hostFromPath(path: String) = path.split("/").last

    private def checkStructure() {
        if (zkClient.exists(StatPath, false) == null) zkClient.create(StatPath, Array[Byte](), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
        if (zkClient.exists(SessionPath, false) == null) zkClient.create(SessionPath, Array[Byte](), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)
    }

    private def addSession(id: String, host: String) = zkClient.create(SessionPath + "/" + id, host.getBytes, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT)

    private def retry(retries: Int)(op: => Unit) {
        var attempt = 0
        var stop = false
        do {
            try {
                op
                stop = true
            } catch {
                case e: KeeperException if e.code() == BADVERSION =>
                    Log.debug("Bad while updating zoo keeper data")
                    if (attempt >= retries) throw e
                    attempt += 1
            }
        } while (!stop)
    }

    private def writeInt(value: Int) = ByteBuffer.allocate(4).putInt(value).array()

    private def doProcessEvent(event: WatchedEvent) = event.getType match {
        case EventType.None if event.getState == KeeperState.Expired =>
            Log.error("ZooKeeper session expired")

        case EventType.None if event.getState == KeeperState.SyncConnected =>
            Log.info("ZooKeeper session established")

            checkStructure()

            // recovery
            (channelCoordinator ? StatRequest).mapTo[StatResponse].onComplete {
                case Failure(ex) =>
                    Log.error("Failed to recover node - unable to read session stats", ex)
                case Success(StatResponse(sessions)) =>
                    sessions.foreach(addSession(_, thisHost))
                    localStats.set(sessions.size)
                    stats(thisHost) = sessions.size
                    zkClient.setData(myPath, writeInt(sessions.size), -1)
            }

            stats ++= zkClient.getChildren(StatPath, true).map(h => (h, readStat(h)))

            Log.debug("Local Node Path: {}", myPath)

        case EventType.NodeDataChanged =>
            val host = hostFromPath(event.getPath)
            val stat = readStat(host)
            Log.debug("Node changed: {} -> {}", host, stat)
            stats += host -> stat

        case EventType.NodeDeleted =>
            val host = hostFromPath(event.getPath)
            stats -= host
            Log.debug("Node removed: {}", host)

        case EventType.NodeChildrenChanged =>
            try {
                val children = zkClient.getChildren(StatPath, true)
                val added = children.filter(!stats.containsKey(_))
                val removed = stats.keys.filter(!children.contains(_))

                if (!added.isEmpty) {
                    Log.debug("Children added: {}", added)
                    stats ++= added.map(h => (h, readStat(h)))
                }

                if (!removed.isEmpty) {
                    Log.debug("Children removed: {}", removed)
                    stats --= removed
                }
            } catch {
                case e: KeeperException if e.code() == CONNECTIONLOSS || e.code() == SESSIONEXPIRED =>
                    Log.info("Connection closed while reading children: {}", StatPath)
            }

        case _ => Log.warn("Unexpected event: {}", event)
    }

    @PreDestroy
    def shutdown() {
        zkClient.close()
    }

    override def process(event: WatchedEvent) {
        try {
            doProcessEvent(event)
        } catch {
            case e: KeeperException => Log.warn("ZooKeeper failure", e)
            case e: Throwable => Log.error("Unexpected error occurred in watcher", e)
        }
    }

    def assignHostForSender(host: String, sender: String) {
        val path = StatPath + "/" + host
        retry(10) {
            val stat = new Stat()
            val counter = readInt(zkClient.getData(path, true, stat))
            zkClient.setData(path, writeInt(counter + 1), stat.getVersion)
        }
        try zkClient.sync(addSession(sender, host), null, null)
        catch {
            case e: NodeExistsException => zkClient.setData(SessionPath + "/" + sender, host.getBytes, -1)
        }
    }

    def removeSenderAssignment(sender: String) {
        retry(10) {
            val stat = new Stat()
            val counter = readInt(zkClient.getData(myPath, true, stat))
            zkClient.setData(myPath, writeInt(counter - 1), stat.getVersion)
        }
        try zkClient.delete(SessionPath + "/" + sender, -1) catch {
            case e: KeeperException => Log.warn("Node for stream '{}' doesn't exists")
        }
    }

    def readSenderAssignment(sender: String) =
        try
            new String(zkClient.getData(SessionPath + "/" + sender, false, null))
        catch {
            case e: KeeperException if e.code() == NONODE => null
        }

    def isHostAvailable(host: String) = {
        zkClient.exists(StatPath + "/" + host, false) != null
    }

    def pickHost = {
        Log.debug("Statistics: {}", stats)
        stats.min._1
    }
}

object ZkNodeMonitor {
    val StatPath = "/stat"
    val SessionPath = "/session"
}