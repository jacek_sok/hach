package edu.pk.hach.server.session.impl

import scala.collection.mutable
import edu.pk.hach.server.session._
import edu.pk.hach.server.session.Session

class LocalRepository(idGenerator: IdGenerator) extends SessionRepository {
    private[server] val storage = mutable.Map[String, Session]()

    def get(id: String): Option[Session] = storage.get(id)

    def get(id: SessionId): (Option[Session], Option[Peer]) = {
        storage.get(id.uuid) match {
            case None => (None, None)
            case session => (session, findPeer(id, session.get))
        }
    }

    def put(session: Session): Session = {
        storage.synchronized {
            val id = if (session.id == null) idGenerator.generateId else {
                if (!storage.contains(session.id)) throw new SessionNotFoundException(session.id)
                session.id
            }
            val result = session.copy(id = id)
            storage(id) = result
            result.copy()
        }
    }

    def put(sessionId: SessionId, peer: Peer): Session = {
        require(sessionId.peer != null)

        storage.synchronized {
            storage.get(sessionId.uuid) match {
                case None => throw new SessionNotFoundException(sessionId)
                case Some(session) =>
                    val newPeersSet = if (session.peers.exists(_.id == sessionId.peer))
                        session.peers.map(p => if (p.id == sessionId.peer) peer.copy() else p)
                    else
                        session.peers + peer
                    val newSession = session.copy(peers = newPeersSet)
                    storage(sessionId.uuid) = newSession
                    newSession.copy()
            }
        }
    }

    def remove(id: String): Option[Session] = storage.synchronized {
        storage.remove(id)
    }

    private def findPeer(sessionId: SessionId, session: Session) = session.peers.find(_.id == sessionId.peer)
}
