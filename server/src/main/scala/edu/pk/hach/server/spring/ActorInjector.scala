package edu.pk.hach.server.spring

import akka.actor.{Actor, IndirectActorProducer}
import org.springframework.context.ApplicationContext

class ActorInjector(context: ApplicationContext, clazz: Class[_ <: Actor]) extends IndirectActorProducer {
    def produce(): Actor = context.getBean(clazz)

    def actorClass: Class[_ <: Actor] = clazz
}
