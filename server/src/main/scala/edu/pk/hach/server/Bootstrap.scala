package edu.pk.hach.server

import org.springframework.context.support.ClassPathXmlApplicationContext
import javax.annotation.{PreDestroy, PostConstruct}
import akka.io.IO
import spray.can.Http
import akka.actor.{ActorRef, ActorSystem}
import org.springframework.beans.factory.annotation.Autowired
import edu.pk.hach.server.configuration.ConfigurationProvider

class Bootstrap @Autowired()(implicit actors: ActorSystem, httpHandler: ActorRef, config: ConfigurationProvider) {
    @PostConstruct
    def start() {
        val cfg = config.getConfiguration
        IO(Http) ! Http.Bind(httpHandler, interface = cfg.interface, port = cfg.port)
    }

    @PreDestroy
    def stop() {
        IO(Http) ! Http.Unbind
    }
}

object Bootstrap extends App {
    try {
        new ClassPathXmlApplicationContext("hach-context.xml").start()
    } catch {
        case e: Exception =>
            e.printStackTrace()
            sys.exit(-1)
    }
}
