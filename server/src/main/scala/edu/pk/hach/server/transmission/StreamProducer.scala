package edu.pk.hach.server.transmission

import akka.actor.{Terminated, ActorRef, Actor}
import spray.http.{ChunkedMessageEnd, MessageChunk}
import edu.pk.hach.protocol.{SessionStatus, PeerStatus, HttpConverters}
import edu.pk.hach.protocol.model.SendStreamResponse
import edu.pk.hach.server.session.{Peer, SessionId, SessionRepository}
import edu.pk.hach.server.transmission.StreamProducer._
import scala.collection.mutable
import org.slf4j.LoggerFactory
import java.util.concurrent.BlockingQueue
import edu.pk.hach.server.cluster.NodeMonitor

class StreamProducer(repository: SessionRepository,
                     nodeMonitor: NodeMonitor,
                     sessionId: SessionId,
                     peer: Peer) extends Actor with HttpConverters {
    private[transmission] val queues = mutable.Map[ActorRef, StreamQueue]()

    override def receive = {
        case (leech: ActorRef, queue: StreamQueue) =>
            Log.debug("Leech connected: {}", leech)
            queues += leech -> queue
            context.watch(leech)

        case chunk: MessageChunk =>
            Log.trace("Message chunk: {}", chunk)
            queues.foreach(e => appendChunk(e._1, e._2, chunk))

        case last: ChunkedMessageEnd =>
            Log.trace("Last chunk: {}", last)
            repository.put(sessionId, peer.copy(status = PeerStatus.Closed, streamHost = null))
            nodeMonitor.removeSenderAssignment(sessionId.peer)
            sender <~ SendStreamResponse(repository.get(sessionId.uuid) match {
                case Some(session) => session.status
                case None => SessionStatus.Closed
            })
            Log.debug("Stopping send stream handler: {}", self)
            context.stop(self)

        case Terminated(leech) =>
            Log.debug("Leech disconnected: {}", leech)
            queues -= leech
    }

    def appendChunk(leech: ActorRef, queue: StreamQueue, chunk: MessageChunk) {
        if (!queue.offer(chunk)) {
            Log.warn("Queue is full for leech: {}", leech)
            queue.poll()
            queue.offer(chunk)
        }
    }
}

object StreamProducer {
    val Log = LoggerFactory.getLogger(classOf[StreamProducer])
    type StreamQueue = BlockingQueue[MessageChunk]
}