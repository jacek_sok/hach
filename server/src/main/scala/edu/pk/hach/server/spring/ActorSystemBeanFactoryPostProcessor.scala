package edu.pk.hach.server.spring

import org.springframework.beans.factory.config._
import org.springframework.core.annotation.AnnotationUtils._
import org.springframework.beans.factory.support.BeanDefinitionRegistry
import org.springframework.beans.factory.support.BeanDefinitionBuilder.rootBeanDefinition
import scala.collection.mutable
import akka.actor.Actor
import org.slf4j.LoggerFactory

class ActorBeanFactoryPostProcessor extends BeanFactoryPostProcessor {
    import ActorBeanFactoryPostProcessor._

    override def postProcessBeanFactory(beanFactory: ConfigurableListableBeanFactory): Unit = {
        val actorSystemBean = getActorSystemBean(beanFactory)
        val registry = beanFactory.asInstanceOf[BeanDefinitionRegistry]

        val beansToAdd = mutable.Map[String, BeanDefinition]()
        val beansToRemove = mutable.ListBuffer[String]()

        for (beanName <- beanFactory.getBeanDefinitionNames) {
            val beanDefinition = beanFactory.getBeanDefinition(beanName)
            val className = beanDefinition.getBeanClassName
            val clazz = Class.forName(className)
            findAnnotation(clazz, classOf[ActorBean]) match {
                case actor: ActorBean =>
                    if (!classOf[Actor].isAssignableFrom(clazz)) {
                        Logger.error("Class '{}' is marked with @ActorBean but it does not extends Actor trait", className)
                    } else {
                        beanDefinition.setScope("prototype")

                        beansToRemove += beanName
                        beansToAdd(beanName + "Actor") = beanDefinition
                        beansToAdd(beanName) = rootBeanDefinition(classOf[ActorFactoryBean])
                            .addConstructorArgReference(actorSystemBean)
                            .addConstructorArgValue(clazz)
                            .addConstructorArgValue(actor)
                            .getBeanDefinition
                    }

                case _ =>
            }
            findAnnotation(clazz, classOf[ImplicitActorSystem]) match {
                case null =>
                case _ =>
                    val beanRef = new RuntimeBeanReference(actorSystemBean)
                    beanDefinition.getConstructorArgumentValues.addGenericArgumentValue(beanRef)
            }
        }

        beansToRemove.foreach(registry.removeBeanDefinition)
        beansToAdd.foreach((b: (String, BeanDefinition)) => registry.registerBeanDefinition(b._1, b._2))
    }

    def getActorSystemBean(beanFactory: ConfigurableListableBeanFactory) =
        beanFactory.getBeanDefinitionNames.filter(beanFactory.getBeanDefinition(_).getBeanClassName == classOf[ActorSystemFactoryBean].getCanonicalName)(0)
}

object ActorBeanFactoryPostProcessor {
    val Logger = LoggerFactory.getLogger(classOf[ActorBeanFactoryPostProcessor])
}