package edu.pk.hach.server.session

class InvalidSessionIdException(string: String) extends Exception("String '%s' is not a valid session id" format string)
