package edu.pk.hach.server.session.impl

import edu.pk.hach.server.session.IdGenerator
import java.util.UUID

class DefaultIdGenerator extends IdGenerator {
    def generateId: String = UUID.randomUUID().toString
}
