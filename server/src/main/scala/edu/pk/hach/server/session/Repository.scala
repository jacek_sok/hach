package edu.pk.hach.server.session

import edu.pk.hach.protocol.SessionStatus.SessionStatusType
import edu.pk.hach.protocol.PeerStatus.PeerStatusType

trait SessionRepository {
    def get(id: String): Option[Session]

    def get(id: SessionId): (Option[Session], Option[Peer])

    def put(session: Session): Session

    def put(sessionId: SessionId, peer: Peer): Session

    def remove(id: String): Option[Session]
}

case class Peer(id: String, name: String, ip: String, streamHost: String, status: PeerStatusType)

case class Session(id: String, originator: String, peers: Set[Peer], status: SessionStatusType)