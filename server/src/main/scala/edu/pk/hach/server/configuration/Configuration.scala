package edu.pk.hach.server.configuration

abstract class Configuration {
    val hostId: String
    val port: Int
    val interface: String
    val db: Db
    val zooKeeper: ZooKeeper
    val nodeSyncTime: Int
    val maxPeers: Int
}

case class Db(host: String, port: Int)

case class ZooKeeper(hosts: List[String], timeout: Int)