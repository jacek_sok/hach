package edu.pk.hach.server

import akka.actor._
import akka.pattern.ask
import spray.can.Http
import org.slf4j.LoggerFactory
import spray.http.StatusCodes._
import edu.pk.hach.protocol.model._
import edu.pk.hach.server.session._
import edu.pk.hach.protocol.{PeerStatus, HttpConverters, SessionStatus}
import spray.http._
import edu.pk.hach.server.spring.{ImplicitActorSystem, ActorBean}
import org.springframework.beans.factory.annotation.Autowired
import edu.pk.hach.server.transmission.{StreamConsumer, StreamProducer}
import edu.pk.hach.server.transmission.ChannelCoordinator._
import edu.pk.hach.server.configuration.ConfigurationProvider
import spray.can.Http.RegisterChunkHandler
import edu.pk.hach.server.session.Session
import edu.pk.hach.protocol.model.InitResponse
import scala.Some
import scala.concurrent.duration.DurationInt
import edu.pk.hach.protocol.model.ErrorResponse
import spray.http.HttpResponse
import edu.pk.hach.protocol.model.StatusResponse
import akka.util.Timeout
import akka.io.Tcp.{ConfirmedClosed, PeerClosed}
import spray.http.Uri.Path
import edu.pk.hach.server.cluster.NodeMonitor
import edu.pk.hach.protocol.meta.Server

@ActorBean(name = "request-handler", dispatcher = "request-dispatcher", hasRouter = true)
@ImplicitActorSystem
class HttpHandler @Autowired()(repository: SessionRepository,
                               idGenerator: IdGenerator,
                               configurationProvider: ConfigurationProvider,
                               channelCoordinator: ActorRef,
                               nodeMonitor: NodeMonitor)
                              (implicit actorSystem: ActorSystem) extends Actor with HttpConverters {
    implicit val timeout = Timeout(5 seconds)
    val cfg = configurationProvider.getConfiguration

    import HttpHandler._
    import actorSystem.dispatcher

    def receive = {
        case m: Http.Connected =>
            Log.trace("Incoming connection ({})", self.path.name)
            sender ! Http.Register(self)

        case m: Http.Bound =>
            Log.info("Bound to address: {}", m.localAddress)

        case PeerClosed | ConfirmedClosed =>
            Log.trace("Peer closed")

        case input: HttpRequestPart =>
            try {
                input match {
                    case rq@StatusRequestFactory(session) => session match {
                        case None => sender <~ ErrorResponse("Empty session ID", rq, BadRequest)
                        case Some(id) =>
                            repository.get(SessionId(id).uuid) match {
                                case None => sender <~ SessionNotFound
                                case Some(Session(_, _, peers, status)) =>
                                    val peersList = peers.map((peer) => PeerInfo(peer.id, peer.name, peer.streamHost))
                                    sender <~ StatusResponse(status, peersList, "POC")
                            }
                    }

                    case rq@InitRequestFactory(multi) =>
                        val originator = idGenerator.generateId
                        val id = repository.put(Session(null, originator, Set(), SessionStatus.Opened)).id
                        sender <~ InitResponse(SessionId(id, originator), SessionId(id, null))

                    case rq@InitPeerRequestFactory(session, name, ip) => sender <~ (
                        if (session == None) ErrorResponse("Empty session ID", rq, BadRequest)
                        else if (name == None) ErrorResponse("Peer name not provided", rq, BadRequest)
                        else {
                            val sessionId = SessionId(session.get)
                            (repository.get(sessionId): @unchecked) match {
                                case (None, None) => SessionNotFound
                                case (Some(sessionData), Some(peer)) =>
                                    ErrorResponse("Already initialized", rq, Locked)
                                case (Some(sessionData), None) if sessionData.peers.size >= cfg.maxPeers =>
                                    ErrorResponse("Peers limit exceeded", rq, Forbidden)
                                case (Some(sessionData), None) =>
                                    val id =
                                        if (sessionId.peer == sessionData.originator) sessionData.originator
                                        else idGenerator.generateId
                                    val peer = Peer(id, name.get, ip, null, null)
                                    val psi = sessionId.copy(peer = id)
                                    repository.put(psi, peer)
                                    InitPeerResponse(psi, peer.id)
                            }
                        }
                        )

                    case rq@SendStreamRequestFactory(session) => session match {
                        case None => sender <~ ErrorResponse("Empty session ID", rq, BadRequest)
                        case Some(id) =>
                            val sessionId = SessionId(id)
                            (repository.get(sessionId): @unchecked) match {
                                case (None, None) =>
                                    sender <~ SessionNotFound
                                case (Some(_), None) =>
                                    sender <~ ErrorResponse("Firstly initialize peer", rq, RetryWith)
                                case (_, Some(Peer(peer, _, _, assignedNode, PeerStatus.Streaming)))
                                    if assignedNode == getHostId || !isDown(assignedNode, peer) =>
                                    sender <~ ErrorResponse("Peer is already streaming", rq, Conflict)
                                case (Some(sessionData), Some(peer)) =>
                                    var assignedNode = nodeMonitor.readSenderAssignment(peer.id)

                                    if (assignedNode == null || !nodeMonitor.isHostAvailable(assignedNode)) {
                                        assignedNode = nodeMonitor.pickHost
                                        nodeMonitor.assignHostForSender(assignedNode, peer.id)
                                    }

                                    if (assignedNode == getHostId) {
                                        val httpSender = sender()
                                        Log.debug("Setting 'write' status to Streaming for peer: {}", peer)
                                        val updatedPeer = peer.copy(streamHost = getHostId, status = PeerStatus.Streaming)
                                        repository.put(sessionId, updatedPeer)
                                        val props = Props.create(classOf[StreamProducer], repository, nodeMonitor, sessionId, updatedPeer)
                                            .withDispatcher(StreamDispatcher)
                                        val producer = actorSystem.actorOf(props)
                                        (channelCoordinator ? SeedConnected(sessionId, producer)).onComplete {
                                            case _ => httpSender ! RegisterChunkHandler(producer)
                                        }
                                    } else
                                        sender <~ RedirectToNodeResponse(Server(assignedNode))
                            }
                    }

                    case rq@ReceiveStreamRequestFactory(session, seed) => session match {
                        case None => sender <~ ErrorResponse("Empty session ID", rq, BadRequest)
                        case Some(id) => seed match {
                            case None =>
                                sender <~ ErrorResponse("Seed ID not provided", rq, BadRequest)
                            case Some(seedId) =>
                                val sessionId = SessionId(SessionId(id).uuid, seedId)
                                (repository.get(sessionId): @unchecked) match {
                                    case (None, None) =>
                                        sender <~ SessionNotFound
                                    case (Some(_), None) =>
                                        sender <~ ErrorResponse("No such channel", rq, NotFound)
                                    case (_, Some(Peer(_, _, _, host, _))) if host != null && host != getHostId =>
                                        if (nodeMonitor.isHostAvailable(host)) {
                                            Log.info("Stream retrieval redirected {}->{}", getHostId, host)
                                            sender <~ RedirectToNodeResponse(Server(host))
                                        } else
                                            sender <~ StreamNotReady
                                    case (Some(sessionData), _) =>
                                        val props = Props.create(classOf[StreamConsumer], sender())
                                        val consumer = actorSystem.actorOf(props)
                                        val response = channelCoordinator ? LeechConnected(sessionId, seed.get, consumer)
                                        val httpSender = sender()

                                        response.onSuccess {
                                            case LeechSuccess(_) =>
                                                httpSender ! ChunkedResponseStart(ReceiveStreamResponse().toHttpResponse)
                                            case LeechFailure(exception) =>
                                                Log.debug("Stream retrieval failed", exception)
                                                httpSender <~ StreamNotReady
                                        }
                                        response.onFailure {
                                            case exception =>
                                                httpSender <~ ErrorResponse(exception.toString, rq)
                                        }
                                }
                        }
                    }

                    case rq@CloseRequestFactory(session) => session match {
                        case None => sender <~ ErrorResponse("Empty session ID", rq, BadRequest)
                        case Some(id) =>
                            val peerSession = SessionId(id)
                            repository.get(peerSession) match {
                                case (Some(data), Some(peer)) =>
                                    if (data.originator == peerSession.peer || data.peers.size == 1) {
                                        repository.remove(peerSession.uuid)
                                    } else {
                                        val newPeers = data.peers.filter(p => p.id != peerSession.peer)
                                        try repository.put(data.copy(peers = newPeers))
                                        catch {
                                            case e: SessionNotFoundException => Log.info("Session already closed: {}", session)
                                        }
                                    }
                                    sender <~ CloseResponse
                                case (Some(data), None) =>
                                    sender <~ InvalidSession
                                case _ =>
                                    sender <~ SessionNotFound
                            }
                    }

                    case PingRequestFactory(_) => sender <~ PingResponse("HACH Server, host=%s, time=%s".format(cfg.hostId, DateTime.now))

                    case HttpRequest(_, Path("/"), _, _, _) => HttpResponse(OK, HttpEntity("HACH - " + cfg.hostId))

                    case _ => sender ! HttpResponse(status = NotFound, entity = "Unknown resource")
                }
            } catch {
                case e: Exception =>
                    Log.error("Unexpected error occurred while handling request: {}", input, e.asInstanceOf[Throwable])
                    sender <~ ErrorResponse(reason = "Unexpected Error", request = input, stackTrace = e.getStackTrace)
            }

        case x => Log.debug(x.toString)
    }

    private def getHostId = {
        configurationProvider.getConfiguration.hostId
    }

    private def isDown(node: String, peer: String) = {
        Log.debug("Checking & Synchronizing for node {}... (node-synchronization-time: {})", node, cfg.nodeSyncTime)
        var done = false
        var time = 0
        while (!done && time < cfg.nodeSyncTime) {
            Thread.sleep(500)
            Log.debug("Checking if assigned host is available: {}", node)
            if (!nodeMonitor.isHostAvailable(node) || nodeMonitor.readSenderAssignment(peer) != node)
                done = true
            time += 500
        }
        Log.debug("Checking result for node {} -> {}", node, done)
        done
    }
}

object HttpHandler {
    val Log = LoggerFactory.getLogger(classOf[HttpHandler])
    val StreamDispatcher = "stream-dispatcher"
}
