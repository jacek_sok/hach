package edu.pk.hach.server.session.impl

import edu.pk.hach.server.session._
import edu.pk.hach.server.configuration.ConfigurationProvider
import collection.JavaConversions._
import com.basho.riak.client.{IRiakObject, RiakFactory}
import edu.pk.hach.protocol.{PeerStatus, SessionStatus}
import edu.pk.hach.server.configuration.impl.TypeSafeConfigurationProvider
import javax.annotation.PreDestroy
import com.basho.riak.client.convert._
import com.basho.riak.client.cap.{Mutation, VClock}
import com.basho.riak.client.query.indexes.RiakIndexes
import com.basho.riak.client.builders.RiakObjectBuilder
import com.basho.riak.client.http.util.Constants
import edu.pk.hach.server.session.Session
import scala.Some
import edu.pk.hach.server.session.Peer

class RiakRepository(configurationProvider: ConfigurationProvider, idGenerator: IdGenerator, cleanup: Boolean = true) extends SessionRepository {
    type StoreType = java.util.Map[String, Object]
    private val cfg = configurationProvider.getConfiguration.db
    private val client = RiakFactory.pbcClient(cfg.host, cfg.port)
    private[impl] val bucket = client.fetchBucket("sessions").execute()
    private[impl] val converter = new SessionConverter("sessions")

    private[impl] def doCleanup() {
        bucket.keys().iterator().foreach(bucket.delete(_).execute())
    }

    if (cleanup) doCleanup()

    @PreDestroy
    def destroy() {
        client.shutdown()
    }

    def get(id: String): Option[Session] = {
        bucket.fetch(id, classOf[Session]).withConverter(converter).execute() match {
            case null => None
            case session => Some(session)
        }
    }

    def get(id: SessionId): (Option[Session], Option[Peer]) = {
        get(id.uuid) match {
            case None => (None, None)
            case Some(session) => (Some(session), session.peers.find(_.id == id.peer))
        }
    }

    def put(session: Session): Session = {
        val id = if (session.id == null) idGenerator.generateId
        else {
            if (bucket.fetch(session.id).execute() == null) throw new SessionNotFoundException(session.id)
            session.id
        }
        val result = session.copy(id = id)
        bucket.store(id, result).withConverter(converter).execute()
        result
    }

    def put(sessionId: SessionId, peer: Peer): Session = {
        require(sessionId.peer != null)
        get(sessionId.uuid) match {
            case None => throw new SessionNotFoundException(sessionId)
            case Some(session) =>
                val newPeersSet = if (session.peers.exists(_.id == sessionId.peer))
                    session.peers.map(p => if (p.id == sessionId.peer) peer.copy() else p)
                else
                    session.peers + peer
                val newSession = session.copy(peers = newPeersSet)
                bucket.store(sessionId.uuid, newSession)
                    .withConverter(converter)
                    .withMutator(new SessionMutation(newSession))
                    .execute()
                newSession
        }
    }

    def remove(id: String): Option[Session] = {
        val session = get(id)
        bucket.delete(id).execute()
        session
    }

    def map2Session(data: StoreType) = {
        val map = mapAsScalaMap(data)
        val peers = map("peers").asInstanceOf[java.util.List[java.util.Map[String, String]]]
        Session(
            map("id").asInstanceOf[String],
            map("originator").asInstanceOf[String],
            peers.map(pm =>
                Peer(
                    pm("id"),
                    pm("name"),
                    pm("ip"),
                    pm("streamHost"),
                    pm.get("status") match {
                        case null => null
                        case status => PeerStatus.withName(status)
                    }
                )
            ).toSet,
            SessionStatus.withName(map("status").asInstanceOf[String])
        )
    }

    def session2json(session: Session) = {
        val peers = if (session.peers != null) session.peers else List[Peer]()
        val peersStrFormat = """{
          |     "id": "%s",
          |     "name": "%s",
          |     "ip": "%s",
          |     "streamHost": %s,
          |     "status": %s
          |}""".stripMargin

        val peersStr = peers.map(peer => peersStrFormat.format(
            peer.id,
            peer.name,
            peer.ip,
            if (peer.streamHost == null) null else "\"" + peer.streamHost + "\"",
            if (peer.status == null) null else "\"" + peer.status.toString + "\""
        )).mkString(",\n")

        """{
           |    "id": "%s",
           |    "originator": "%s",
           |    "status": "%s",
           |    "peers": [
           |        %s
           |    ]
           |}""".stripMargin.format(
                session.id,
                session.originator,
                session.status.toString,
                peersStr
            )
    }

    class SessionConverter(bucket: String) extends Converter[Session] {
        private def jsonConverter(key: String = null) = new JSONConverter(classOf[StoreType], bucket, key)
        val usermetaConverter = new UsermetaConverter[Session]()
        val riakIndexConverter = new RiakIndexConverter[Session]()
        val riakLinksConverter = new RiakLinksConverter[Session]()

        override def fromDomain(domainObject: Session, vclock: VClock): IRiakObject = {
            val key = domainObject.id
            val value = session2json(domainObject)
            val usermetaData = usermetaConverter.getUsermetaData(domainObject)
            val indexes: RiakIndexes = riakIndexConverter.getIndexes(domainObject)
            val links = riakLinksConverter.getLinks(domainObject)
            RiakObjectBuilder.newBuilder(bucket, key)
                .withValue(value)
                .withVClock(vclock)
                .withUsermeta(usermetaData)
                .withIndexes(indexes)
                .withLinks(links)
                .withContentType(Constants.CTYPE_JSON_UTF8)
                .build()
        }

        override def toDomain(riakObject: IRiakObject): Session =
            if (riakObject == null) null
            else map2Session(jsonConverter().toDomain(riakObject))
    }

    class SessionMutation(session: Session) extends Mutation[Session] {
        override def apply(original: Session): Session = session.copy(peers = session.peers ++ original.peers)
    }
}

object AdminTool extends App {
    val cfg = new TypeSafeConfigurationProvider
    val repo = new RiakRepository(cfg, new DefaultIdGenerator, false)
    try {
        var quit = false

        do {
            menu()
            readChar() match {
                case '1' =>
                    println("Number of stored sessions: %d\n".format(count))
                case '2' =>
                    println("Provide session id")
                    print("\t> ")
                    println("Session:\n" + session(readLine()))
                case '3' =>
                    purge()
                    println("Done")
                case 'q' =>
                    quit = true
            }
            println("\n")
        } while (!quit)

        def count = {
            repo.bucket.keys().iterator().count(_ => true)
        }

        def session(id: String) = {
            repo.get(id)
        }

        def purge() {
            repo.doCleanup()
        }
    } finally {
        repo.destroy()
    }

    def menu() {
        println("Riak Admin Tool - select operation:")
        println("1) count stored sessions")
        println("2) read session")
        println("3) purge data")
        println("q) quit")
        print("\t> ")
    }
}