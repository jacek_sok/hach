package edu.pk.hach.server.spring

import org.springframework.context.{ApplicationContextAware, ApplicationContext}
import akka.actor.{ActorRef, Props, ActorSystem}
import org.springframework.beans.factory.FactoryBean
import java.util.concurrent.atomic.AtomicReference
import akka.routing.FromConfig
import org.springframework.util.StringUtils.isEmpty

class ActorFactoryBean(actors: ActorSystem, clazz: Class[_], cfg: ActorBean) extends FactoryBean[ActorRef] with ApplicationContextAware {
    private val ctx = new AtomicReference[ApplicationContext]()

    override def isSingleton: Boolean = true

    override def getObjectType: Class[_] = classOf[ActorRef]

    override def getObject: ActorRef = {
        var props = Props.create(classOf[ActorInjector], ctx.get(), clazz)
        if (cfg.hasRouter) props = props.withRouter(FromConfig())
        if (!isEmpty(cfg.dispatcher())) props = props.withDispatcher(cfg.dispatcher())
        if (isEmpty(cfg.name())) actors.actorOf(props) else actors.actorOf(props, cfg.name())
    }

    override def setApplicationContext(applicationContext: ApplicationContext): Unit = {
        ctx.set(applicationContext)
    }
}
