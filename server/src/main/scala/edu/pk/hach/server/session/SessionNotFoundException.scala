package edu.pk.hach.server.session

class SessionNotFoundException(id: String) extends Exception("Session '%s' not found" format id)
