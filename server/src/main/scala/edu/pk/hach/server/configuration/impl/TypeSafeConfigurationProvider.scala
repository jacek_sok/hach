package edu.pk.hach.server.configuration.impl

import edu.pk.hach.server.configuration.{ZooKeeper, Db, Configuration, ConfigurationProvider}
import com.typesafe.config.ConfigFactory
import java.net.InetAddress
import scala.collection.JavaConversions._

class TypeSafeConfigurationProvider extends ConfigurationProvider {
    private lazy val configuration = new Configuration {
        private val conf = ConfigFactory.load().getConfig("hach.server")

        val interface: String = conf.getString("interface")
        val port: Int = conf.getInt("port")
        val hostId: String = InetAddress.getLocalHost.getHostName + ":" + port

        private val dbCfg = conf.getConfig("db")
        val db = Db(dbCfg.getString("host"), dbCfg.getInt("port"))

        private val zkCfg = conf.getConfig("zk")
        val zooKeeper = ZooKeeper(zkCfg.getStringList("hosts").toList, zkCfg.getInt("timeout"))

        val nodeSyncTime = conf.getInt("node-synchronization-time")

        val maxPeers = conf.getInt("max-peers")
    }

    override def getConfiguration = configuration
}
