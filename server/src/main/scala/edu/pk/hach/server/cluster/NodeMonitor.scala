package edu.pk.hach.server.cluster

trait NodeMonitor {
    def assignHostForSender(host: String, sender: String): Unit
    def removeSenderAssignment(sender: String): Unit
    def isHostAvailable(host: String): Boolean
    def readSenderAssignment(sender: String): String
    def pickHost: String
}
