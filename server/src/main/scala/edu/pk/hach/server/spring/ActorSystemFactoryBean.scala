package edu.pk.hach.server.spring

import org.springframework.beans.factory.FactoryBean
import akka.actor.ActorSystem

class ActorSystemFactoryBean(name: String) extends FactoryBean[ActorSystem] {
    override def isSingleton: Boolean = true

    override def getObjectType: Class[_] = classOf[ActorSystem]

    override def getObject: ActorSystem = ActorSystem(name)
}
