package edu.pk.hach.server.session

trait IdGenerator {
    def generateId: String
}
