package edu.pk.hach.server.cluster.impl

import edu.pk.hach.server.cluster.NodeMonitor
import scala.collection.mutable
import edu.pk.hach.server.configuration.ConfigurationProvider

class LocalNodeMonitor(configurationProvider: ConfigurationProvider) extends NodeMonitor {
    private val hostId = configurationProvider.getConfiguration.hostId
    private val hosts = mutable.Set[String](hostId)
    private val sessions = mutable.Map[String, String]()

    override def assignHostForSender(host: String, sender: String): Unit = sessions(sender) = host

    override def pickHost: String = hosts.map(h => (h, sessions.count(s => s._2 == h))).minBy(_._2)._1

    override def readSenderAssignment(sender: String): String = sessions.getOrElse(sender, null)

    override def removeSenderAssignment(sender: String): Unit = sessions -= sender

    override def isHostAvailable(host: String): Boolean = hosts.contains(host)

    def addHost(host: String) {
        hosts += host
    }
}
