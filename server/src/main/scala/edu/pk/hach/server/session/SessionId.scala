package edu.pk.hach.server.session

import org.jasypt.exceptions.EncryptionOperationNotPossibleException
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor
import org.jasypt.salt.ZeroSaltGenerator

case class SessionId(uuid: String, peer: String){
    override def toString: String = SessionId.sessionId2String(this)

    override def equals(obj: scala.Any): Boolean = obj match {
        case s: String => toString == s
        case SessionId(u, p) => uuid == u && peer == p
        case _ => false
    }
}

object SessionId {
    private val Separator = '|'
    private val Encryptor = {
        val bte = new StandardPBEStringEncryptor()
        bte.setSaltGenerator(new ZeroSaltGenerator)
        bte.setPassword("hach")
        bte
    }

    def apply(session: String) = try {
        val data = Encryptor.decrypt(session).split(Separator)
        new SessionId(data(0), data(1))
    } catch {
        case e: EncryptionOperationNotPossibleException => throw new InvalidSessionIdException(session)
    }

    implicit def sessionId2String(sessionId: SessionId): String = Encryptor.encrypt("%s%s%s".format(sessionId.uuid, Separator, sessionId.peer))
}
