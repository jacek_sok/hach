package edu.pk.hach.server.spring;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ActorBean {
    String name() default "";
    String dispatcher() default "";
    boolean hasRouter() default false;
}
