package edu.pk.hach.server.fixture

import spray.http.Uri.Path
import spray.http._
import spray.http.HttpHeaders.Host
import org.scalatest.concurrent.ScalaFutures
import spray.http.HttpRequest
import org.scalatest.Assertions
import scala.util.matching.Regex
import edu.pk.hach.protocol.EntityMarshaller

trait HttpFixture extends ScalaFutures with Assertions {
    val hostHeader = Host("localhost", 10123)

    def request(method: HttpMethod, path: String, params: Map[String, String] = Map(), headers: List[HttpHeader] = List()) = {
        val uri = Uri./.withPath(Path(path))
        val entity = HttpEntity(EntityMarshaller.marshall(params))
        HttpRequest(method, uri, headers, entity)
    }

    def request(httpRequest: HttpRequest): HttpRequest = httpRequest.copy(headers = hostHeader :: httpRequest.headers)

    implicit def httpResponseMatcher(httpResponse: HttpResponse): HttpResponseMatcher = new HttpResponseMatcher(httpResponse)

    implicit def stringToConstMatcher(string: String): ConstMatcher = new ConstMatcher(string)

    implicit def regexToRegexMatcher(regex: Regex): RegexMatcher = new RegexMatcher(regex)

    val entity = {}

    def status(status: StatusCode) = status

    def contain(body: String) = body

    def matchRegex(regex: Regex) = regex

    def parameters(params: (String, ParameterMatcher)*) = params

    class HttpResponseMatcher(httpResponse: HttpResponse) {
        type EntityType = Unit
        type StatusType = StatusCode
        type ParameterType = (String, ParameterMatcher)
        type ParametersType = Seq[ParameterType]

        private lazy val parameters = EntityMarshaller.unmarshall(httpResponse.entity)

        def shouldHave(entity: EntityType) = {
            assert(!httpResponse.entity.asString.isEmpty, "Expected to have non-empty entity but it's empty")
            this
        }

        def shouldHave(params: ParametersType) = {
            for ((name, matcher) <- params) {
                if (parameters.contains(name))
                    assert(matcher.matches(parameters(name).asInstanceOf[String]), "Expected '%s' to be '%s' but is '%s'" format(name, matcher, parameters(name)))
                else
                    fail("Expected parameter '%s' with value '%s' is not present" format(name, matcher))
            }
            this
        }

        def shouldHave(status: StatusType) = {
            assert(httpResponse.status == status, "Expected status to be '%s' but is '%s'" format(status, httpResponse.status))
            this
        }

        def should(regex: Regex) = {
            assert(regex.matches(httpResponse.entity.asString), "Response body does not match expected regex: '%s'" format regex.pattern)
            this
        }

        def should(body: String) = {
            assert(httpResponse.entity.asString.contains(body), "Response body does not contain expected excerpt: '%s'" format body)
            this
        }
    }

    trait ParameterMatcher {
        def matches(value: String): Boolean
    }

    class ConstMatcher(expected: String) extends ParameterMatcher {
        override def matches(value: String): Boolean = expected == value

        override def toString: String = expected
    }

    class RegexMatcher(expected: Regex) extends ParameterMatcher {
        override def matches(value: String): Boolean = expected.pattern.matcher(value).matches()

        override def toString: String = expected.toString()
    }

}
