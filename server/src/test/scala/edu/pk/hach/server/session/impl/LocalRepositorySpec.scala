package edu.pk.hach.server.session.impl

import edu.pk.hach.server.UnitSpec
import edu.pk.hach.server.session.{SessionNotFoundException, Peer, SessionId, Session}
import edu.pk.hach.protocol.{PeerStatus, SessionStatus}
import java.util.concurrent.Executors
import scala.util.Random

class LocalRepositorySpec extends UnitSpec {
    val dummyPeer = Peer("a", "me", "127.0.0.1", "localhost", PeerStatus.Ready)
    val dummySession = Session(null, "a", Set(dummyPeer), SessionStatus.Opened)
    val idGenerator = new DefaultIdGenerator

    "get(String)" should "give session when it exists" in {
        val repo = new LocalRepository(idGenerator)
        val sid = repo.put(dummySession).id

        val session = repo.get(sid)

        session should be(Some(dummySession.copy(id = sid)))
    }
    it should "give None when session doesn't exists" in {
        val repo = new LocalRepository(idGenerator)

        val session = repo.get("1234")

        session should be(None)
    }

    "get(SessionId)" should "give session when it exists" in {
        val repo = new LocalRepository(idGenerator)
        val sid = repo.put(dummySession).id

        val (s, p) = repo.get(SessionId(sid, dummyPeer.id))

        s should equal(Some(dummySession.copy(id = sid)))
        p should equal(Some(dummyPeer))
    }
    it should "give None when session doesn't exists" in {
        val repo = new LocalRepository(idGenerator)

        val session = repo.get(SessionId("2", "b"))

        session should be((None, None))
    }

    "put(Session)" should "add new session and generate ID when provided session ID is null" in {
        val repo = new LocalRepository(idGenerator)
        val session = dummySession

        val result = repo.put(session)

        result should not be null
        result.id.toString should (not be null and not be empty)
    }
    it should "update session when session ID exists" in {
        val repo = new LocalRepository(idGenerator)
        val old = repo.put(dummySession)
        val session = dummySession.copy(id = old.id, originator = "2")

        val result = repo.put(session)

        result should not be null
        result should not equal old
        Some(result) should equal(repo.get(old.id))
    }
    it should "throw exception when session ID doesn't exists" in {
        val repo = new LocalRepository(idGenerator)
        intercept[SessionNotFoundException] {
            repo.put(dummySession.copy(id = SessionId("2", "b")))
        }
    }

    "put(SessionId,Peer)" should "update add peer data to session" in {
        val repo = new LocalRepository(idGenerator)
        val sid = SessionId(repo.put(dummySession).id, "b")
        val newPeer = dummyPeer.copy(id = "b", name = "you")

        val session = repo.put(sid, newPeer)

        session should not be null
        session.peers should have size 2
        session.peers should contain(newPeer)

    }
    it should "update peer data in session" in {
        val repo = new LocalRepository(idGenerator)
        val sid = SessionId(repo.put(dummySession).id,  "a")
        val updatedPeer = dummyPeer.copy(name = "XXX")

        val session = repo.put(sid, updatedPeer)

        session should not be null
        session.peers should have size 1
        session.peers should contain(updatedPeer)
    }
    it should "throw exception when session doesn't exists" in {
        val repo = new LocalRepository(idGenerator)
        intercept[SessionNotFoundException] {
            repo.put(SessionId("2", "b"), dummyPeer)
        }
    }

    "Repository" should "stay stable when accessed by multiple threads" in {
        val repo = new LocalRepository(idGenerator)

        val session = dummySession.copy(id = SessionId("1", "a"))

        val pool = Executors.newFixedThreadPool(100)
        (for (i <- 0 to 10000) yield pool.submit(new Runnable {
            override def run(): Unit = {
                var s = session
                try {
                    val random = new Random(System.currentTimeMillis())
                    for (i <- 1 to 1000000) {
                        if (random.nextBoolean()) {
                            val x = s.copy(originator = idGenerator.generateId)
                            repo.put(s).originator should equal(x.originator)
                        } else {
                            repo.remove(s.id)
                        }
                    }
                } catch {
                    case e: SessionNotFoundException => s = repo.put(s.copy(id = null))
                    case e: Exception => fail(e)
                }
            }
        })).foreach(_.get())
    }
}
