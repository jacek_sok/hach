package edu.pk.hach.server.transmission

import edu.pk.hach.server.UnitSpec
import edu.pk.hach.server.transmission.ChannelCoordinator._
import edu.pk.hach.server.session.SessionId
import akka.testkit.{TestProbe, TestActorRef}
import edu.pk.hach.server.fixture.AkkaFixture

class ChannelCoordinatorSpec extends UnitSpec with AkkaFixture {
    val coordinator = TestActorRef(new ChannelCoordinator(system))
    val coordinatorObj = coordinator.underlyingActor
    before {
        import coordinatorObj._
        session2seed.clear()
        seed2session.clear()
    }

    "ChannelCoordinator" should "add and watch producer" in {
        import coordinatorObj._
        assume(session2seed.isEmpty)
        assume(seed2session.isEmpty)

        val seed = createTestActor

        val id = SessionId("id", "me")

        coordinator ! SeedConnected(id, seed)

        session2seed should have size 1
        seed2session should have size 1

        seed.stop()

        session2seed should be(empty)
        seed2session should be(empty)
    }
    it should "connect leech to seed's channel" in {
        val seed = TestProbe()
        val leech = TestProbe()

        val seedSession = SessionId("id", "seed")
        val leechSession = SessionId("id", "leech")

        coordinator ! SeedConnected(seedSession, seed.ref)
        coordinator ! LeechConnected(leechSession, seedSession.peer, leech.ref)

        expectMsg("OK")
        expectMsg(LeechSuccess(seed.ref))
        leech.expectMsg(seed.ref)
    }
    it should "fail to connect leech when channel doesn't exists" in {
        val leech = createTestActor

        val seedSession = SessionId("id", "seed")
        val leechSession = SessionId("id", "leech")

        coordinator ! LeechConnected(leechSession, seedSession.peer, leech)

        expectMsgClass(classOf[LeechFailure])
    }
    it should "return peers list when asked for" in {
        coordinator ! SeedConnected(SessionId("a", "1"), TestProbe().ref)
        coordinator ! SeedConnected(SessionId("b", "2"), TestProbe().ref)
        coordinator ! SeedConnected(SessionId("c", "3"), TestProbe().ref)
        coordinator ! SeedConnected(SessionId("d", "4"), TestProbe().ref)
        expectMsgAllOf("OK", "OK", "OK", "OK")

        coordinator ! StatRequest

        val rs = expectMsgClass(classOf[StatResponse])
        rs.sessions should contain allOf("1", "2", "3", "4")
    }
}
