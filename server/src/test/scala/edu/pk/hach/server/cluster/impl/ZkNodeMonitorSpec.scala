package edu.pk.hach.server.cluster.impl

import edu.pk.hach.server.UnitSpec
import org.apache.zookeeper.server.{NIOServerCnxnFactory, ZooKeeperServer}
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll}
import edu.pk.hach.server.configuration._
import scala.reflect.io.Directory
import java.net.InetSocketAddress
import akka.actor.{Props, Actor, ActorRef}
import org.apache.zookeeper.data.Stat
import edu.pk.hach.server.configuration.Db
import edu.pk.hach.server.configuration.ZooKeeper
import edu.pk.hach.server.transmission.ChannelCoordinator.{StatRequest, StatResponse}
import java.util.UUID
import java.lang.Thread.sleep

class ZkNodeMonitorSpec extends UnitSpec with BeforeAndAfterAll with BeforeAndAfter {
    val zkServer = new ZooKeeperServer(Directory.makeTemp().jfile, Directory.makeTemp().jfile, 100)
    val factory = new NIOServerCnxnFactory()
    var monitors = List[ZkNodeMonitor]()

    override protected def beforeAll() {
        factory.configure(new InetSocketAddress(22156), 10)
        factory.startup(zkServer)
    }

    override protected def afterAll() {
        factory.shutdown()
    }

    after {
        monitors.foreach(_.shutdown())
        sleep(100)
    }

    def openMonitor(cfg: ConfigurationProvider, coordinator: ActorRef) = {
        val monitor = new ZkNodeMonitor(cfg, coordinator)
        monitors = monitor :: monitors
        sleep(100)
        monitor
    }

    class TestConfigProvider(host: String = "local") extends ConfigurationProvider {
        def getConfiguration = {
            new Configuration {
                val interface: String = null
                val hostId: String = host
                val db: Db = null
                val zooKeeper: ZooKeeper = ZooKeeper(List("localhost:" + zkServer.getClientPort), 2000)
                val port: Int = 0
                val nodeSyncTime = 100
                val maxPeers = 10
            }
        }
    }

    class TestCoordinator(sessions: Int) extends Actor {
        override def receive = {
            case StatRequest => sender ! StatResponse((for (i <- 1 to sessions) yield UUID.randomUUID().toString).toList)
        }
    }

    object TestCoordinator {
        def apply(sessions: Int) = system.actorOf(Props(classOf[TestCoordinator], ZkNodeMonitorSpec.this, sessions))
    }

    lazy val cfg = new TestConfigProvider
    lazy val coordinator = TestCoordinator(0)
    "Node Monitoring" should "setup environment on startup" in {
        openMonitor(cfg, coordinator)

        zkServer.getZKDatabase.getSessions.size() should equal(1)

        zkServer.getZKDatabase.getDataTree.getNode(ZkNodeMonitor.StatPath) should not be null
        zkServer.getZKDatabase.getDataTree.getNode(ZkNodeMonitor.SessionPath) should not be null
    }

    "Host Assignment" should "add and remove stream info" in {
        val monitor = openMonitor(cfg, coordinator)

        monitor.assignHostForSender(cfg.getConfiguration.hostId, "1")
        monitor.assignHostForSender(cfg.getConfiguration.hostId, "2")
        monitor.assignHostForSender(cfg.getConfiguration.hostId, "3")
        monitor.removeSenderAssignment("2")

        zkServer.getZKDatabase.getData(ZkNodeMonitor.StatPath + "/local", new Stat(), null) should be equals "2".getBytes
        zkServer.getZKDatabase.getDataTree.getNode(ZkNodeMonitor.SessionPath + "/1") should not be null
        zkServer.getZKDatabase.getDataTree.getNode(ZkNodeMonitor.SessionPath + "/2") should be (null)
        zkServer.getZKDatabase.getDataTree.getNode(ZkNodeMonitor.SessionPath + "/3") should not be null
    }
    it should "return null when no node assigned for stream" in {
        val monitor = openMonitor(cfg, coordinator)

        monitor.readSenderAssignment("random-id") should be (null)
    }
    it should "return assigned node for stream" in {
        openMonitor(new TestConfigProvider("A"), coordinator)
        val monitor = openMonitor(cfg, coordinator)

        monitor.assignHostForSender("A", "1")

        monitor.readSenderAssignment("1") should equal("A")
    }

    "Host availability" should "return true when node exists" in {
        val m1 = openMonitor(new TestConfigProvider("h1"), coordinator)
        val m2 = openMonitor(new TestConfigProvider("h2"), coordinator)

        m1.isHostAvailable("h1") should equal(true)
        m1.isHostAvailable("h2") should equal(true)
        m2.isHostAvailable("h1") should equal(true)
        m2.isHostAvailable("h2") should equal(true)
    }
    it should "return false when node disconnected" in {
        val m1 = openMonitor(new TestConfigProvider("h1"), coordinator)
        val m2 = openMonitor(new TestConfigProvider("h2"), coordinator)

        m1.shutdown()

        m2.isHostAvailable("h1") should equal(false)
        m2.isHostAvailable("h2") should equal(true)
    }

    "Host pickup" should "return host with lowest utilization" in {
        val nodesMonitors = Seq(
            openMonitor(new TestConfigProvider("h1"), TestCoordinator(13)),
            openMonitor(new TestConfigProvider("h2"), TestCoordinator(15)),
            openMonitor(new TestConfigProvider("h3"), TestCoordinator(19)),
            openMonitor(new TestConfigProvider("h4"), TestCoordinator(7))
        )

        nodesMonitors.foreach(_.pickHost should be("h4"))
    }
    it should "return host with lowest utilization after synchronization" in {
        val nodesMonitors = Seq(
            openMonitor(new TestConfigProvider("h1"), TestCoordinator(13)),
            openMonitor(new TestConfigProvider("h2"), TestCoordinator(15)),
            openMonitor(new TestConfigProvider("h3"), TestCoordinator(9)),
            openMonitor(new TestConfigProvider("h4"), TestCoordinator(10))
        )

        nodesMonitors.foreach(_.pickHost should be("h3"))

        nodesMonitors(2).shutdown()

        nodesMonitors.filterNot(_.eq(nodesMonitors(2))).foreach(_.pickHost should be("h4"))
    }
}
