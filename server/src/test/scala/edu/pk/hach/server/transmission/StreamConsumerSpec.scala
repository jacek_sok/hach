package edu.pk.hach.server.transmission

import edu.pk.hach.server.UnitSpec
import akka.testkit.TestActorRef
import spray.http.{ChunkedMessageEnd, MessageChunk}
import edu.pk.hach.server.fixture.AkkaFixture
import akka.actor.ActorRef
import edu.pk.hach.server.transmission.StreamProducer.StreamQueue

class StreamConsumerSpec extends UnitSpec with AkkaFixture {
    "StreamConsumer" should "create and terminate properly" in {
        val seed = createTestActor

        val consumer = TestActorRef(new StreamConsumer(self))

        consumer ! seed
        seed.stop()

        expectMsgClass(classOf[ChunkedMessageEnd])
    }
    it should "forward message chunks" in {
        val consumer = TestActorRef(new StreamConsumer(self))

        consumer ! self
        val queue = expectMsgClass(classOf[(ActorRef, StreamQueue)])._2

        queue.offer(MessageChunk("J")) should be(true)
        queue.offer(MessageChunk("A")) should be(true)

        expectMsg(MessageChunk("J"))
        expectMsg(MessageChunk("A"))
    }
}
