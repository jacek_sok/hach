package edu.pk.hach.server

import edu.pk.hach.server.session.{Peer, SessionId, Session}
import akka.testkit.{TestProbe, TestActorRef}
import edu.pk.hach.server.fixture.HttpFixture
import edu.pk.hach.protocol.model._
import spray.http.{ChunkedResponseStart, ChunkedRequestStart, StatusCodes, HttpResponse}
import org.mockito.Mockito._
import org.mockito.Matchers._
import edu.pk.hach.server.session.impl.{DefaultIdGenerator, LocalRepository}
import edu.pk.hach.protocol.SessionStatus._
import edu.pk.hach.server.configuration.impl.TypeSafeConfigurationProvider
import edu.pk.hach.protocol.{SessionStatus, PeerStatus}
import edu.pk.hach.protocol.EntityMarshaller._
import edu.pk.hach.protocol.model.StatusRequest
import edu.pk.hach.protocol.model.InitRequest
import edu.pk.hach.server.transmission.ChannelCoordinator.{LeechSuccess, LeechFailure, LeechConnected, SeedConnected}
import spray.can.Http.RegisterChunkHandler
import edu.pk.hach.server.cluster.NodeMonitor
import edu.pk.hach.protocol.meta.Server

class HttpHandlerSpec extends UnitSpec with HttpFixture {
    val idGenerator = new DefaultIdGenerator
    val nodeMonitor = mock(classOf[NodeMonitor])
    val repository = spy(new LocalRepository(idGenerator))
    val coordinator = TestProbe()
    val config = new TypeSafeConfigurationProvider
    val handler = TestActorRef(new HttpHandler(repository, idGenerator, config, coordinator.ref, nodeMonitor))
    val dummySession = Session(idGenerator.generateId, "me", Set(), Opened)
    when(repository.get(dummySession.id)).thenReturn(Some(dummySession))

    before {
        repository.storage.clear()
        reset(repository)
        reset(nodeMonitor)
        when(nodeMonitor.pickHost).thenReturn(config.getConfiguration.hostId)
    }

    "/status" should "return error response when session id is not provided" in {
        handler ! request(StatusRequestFactory.Method, StatusRequestFactory.Url)

        val rs = expectMsgClass(classOf[HttpResponse])
        assertEmptySessionID(rs)
    }
    it should "return error response when session not found" in {
        val sessionId = SessionId(idGenerator.generateId, null).toString
        doReturn(None).when(repository).get(sessionId)

        handler ! StatusRequest(sessionId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        assertSessionNotFound(rs)
    }
    it should "return session status response" in {
        val newSession = repository.put(dummySession.copy(id = null))

        handler ! StatusRequest(SessionId(newSession.id, null)).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.OK)
        rs shouldHave parameters("status" -> Opened.toString, "serverString" -> ".+".r)
        verify(repository).get(newSession.id)
    }

    "/init" should "initialize session and return session id for user and exchange key" in {
        handler ! InitRequest().toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.OK)
        rs shouldHave parameters("session" -> ".+".r, "exchange" -> ".+".r)
        verify(repository).put(any(classOf[Session]))

        val sessionId = getSessionId(rs)
        val session = repository.get(sessionId.uuid)
        session should not be None
        session.get.peers should be(empty)
        session.get.originator should equal(sessionId.peer)
        session.get.status should equal(SessionStatus.Opened)
    }

    it should "initialize peer and return session id for user" in {
        val newSession = repository.put(dummySession.copy(id = null))

        handler ! InitPeerRequest(SessionId(newSession.id, null), "me").toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.OK)
        rs shouldHave parameters("session" -> ".+".r)

        val sessionId = getSessionId(rs)
        val session = repository.get(sessionId.uuid)
        session should not be None
        session.get.peers should have size 1
        val peer = session.get.peers.find(_ => true).get
        peer.name should equal("me")
        peer.id should equal(sessionId.peer)
        peer.streamHost should be(null)
    }
    it should "initialize originator" in {
        val peerId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(id = null, originator = peerId, peers = Set(Peer("a", "b", "c", "d", null))))
        val sessionId = SessionId(newSession.id, peerId)

        handler ! InitPeerRequest(sessionId, "orig").toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.OK)
        rs shouldHave parameters("session" -> ".+".r)


        val session = repository.get(sessionId.uuid)
        session should not be None
        session.get.peers should have size 2

        val newPeer = session.get.peers.find(_.id == peerId)
        newPeer should equal(Some(Peer(peerId, "orig", "localhost", null, null)))
    }
    it should "return error response when session id is not provided" in {
        handler ! request(InitPeerRequestFactory.Method, InitPeerRequestFactory.Url)

        val rs = expectMsgClass(classOf[HttpResponse])
        assertEmptySessionID(rs)
    }
    it should "return error response when session not found" in {
        handler ! InitPeerRequest(SessionId(idGenerator.generateId, null), "orig").toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        assertSessionNotFound(rs)
    }
    it should "return error response when peer name not provided" in {
        handler ! request(InitPeerRequestFactory.Method, InitPeerRequestFactory.Url + "/123")

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.BadRequest)
        rs should contain("Peer name")
        verifyZeroInteractions(repository)
    }
    it should "return error response peer already initialized" in {
        val peerId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(id = null, peers = Set(Peer(peerId, "b", "c", "d", null))))
        val sessionId = SessionId(newSession.id, peerId)

        handler ! InitPeerRequest(sessionId, "ae").toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.Locked)
        rs should contain("Already initialized")
    }
    it should "return error response peers count exceeded" in {
        val peerId = idGenerator.generateId
        val peersSet = Set(Peer("a", "b", "c", "d", null), Peer("1", "2", "3", "4", null), Peer("!", "@", "$", "%", null))
        val newSession = repository.put(dummySession.copy(id = null, originator = peerId, peers = peersSet))
        val sessionId = SessionId(newSession.id, peerId)

        handler ! InitPeerRequest(sessionId, "exceed").toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.Forbidden)
        rs should contain("Peers limit exceeded")
    }

    "/stream (write)" should "start sending stream" in {
        val peerId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(id = null, peers = Set(Peer(peerId, "b", "c", "d", null))))
        val sessionId = SessionId(newSession.id, peerId)

        handler ! SendStreamRequest(sessionId).toHttpRequest

        val event = coordinator.expectMsgClass(classOf[SeedConnected])
        event.session should equal(sessionId)
        event.producer should not be null
        coordinator.reply("OK")

        expectMsgClass(classOf[RegisterChunkHandler])
        Thread.sleep(100)
    }
    it should "pick new node and redirect when selected is down" in {
        val peerId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(id = null, peers = Set(Peer(peerId, "b", "c", "d", null))))
        val sessionId = SessionId(newSession.id, peerId)

        when(nodeMonitor.readSenderAssignment(peerId)).thenReturn("down")
        when(nodeMonitor.isHostAvailable("down")).thenReturn(false)
        when(nodeMonitor.pickHost).thenReturn("up")

        handler ! SendStreamRequest(sessionId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        val redirect = RedirectToNodeResponseFactory.unapply(rs)
        redirect should be (Some(Server("up", 80)))
    }
    it should "return error response when session id is not provided" in {
        handler ! ChunkedRequestStart(request(SendStreamRequestFactory.Method, SendStreamRequestFactory.Url))

        val rs = expectMsgClass(classOf[HttpResponse])
        assertEmptySessionID(rs)
    }
    it should "return error response when session not found" in {
        handler ! SendStreamRequest(SessionId(idGenerator.generateId, null)).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        assertSessionNotFound(rs)
    }

    it should "return error response when peer not initialized" in {
        val peerId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(id = null))
        val sessionId = SessionId(newSession.id, peerId)

        handler ! SendStreamRequest(sessionId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.RetryWith)
        rs should contain("initialize peer")
    }
    it should "return error response when peer is already streaming" in {
        val peerId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(id = null, peers = Set(Peer(peerId, "b", "c", config.getConfiguration.hostId, PeerStatus.Streaming))))
        val sessionId = SessionId(newSession.id, peerId)

        handler ! SendStreamRequest(sessionId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.Conflict)
        rs should contain("already streaming")
    }

    "/stream (read)" should "start read stream" in {
        val leechId = idGenerator.generateId
        val seedId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(
            id = null,
            peers = Set(Peer(leechId, "b", "c", "d", null), Peer(seedId, "b", "c", config.getConfiguration.hostId, PeerStatus.Streaming))
        ))
        val sessionId = SessionId(newSession.id, leechId)

        handler ! ReceiveStreamRequest(sessionId, seedId).toHttpRequest

        val event = coordinator.expectMsgClass(classOf[LeechConnected])
        event.session.uuid should equal(sessionId.uuid)
        event.seed should equal(seedId)
        event.actor should not be null

        coordinator.reply(LeechSuccess(null))

        expectMsgClass(classOf[ChunkedResponseStart])
        Thread.sleep(100)
    }
    it should "redirect to the hosting node" in {
        val leechId = idGenerator.generateId
        val seedId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(
            id = null,
            peers = Set(Peer(leechId, "b", "c", "d", null), Peer(seedId, "e", "f", "g", PeerStatus.Streaming))
        ))
        val sessionId = SessionId(newSession.id, leechId)
        when(nodeMonitor.isHostAvailable("g")).thenReturn(true)

        handler ! ReceiveStreamRequest(sessionId, seedId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        val redirect = RedirectToNodeResponseFactory.unapply(rs)
        redirect should be (Some(Server("g", 80)))
    }
    it should "return stream not ready response when hosting node is down" in {
        val leechId = idGenerator.generateId
        val seedId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(
            id = null,
            peers = Set(Peer(leechId, "b", "c", "d", null), Peer(seedId, "e", "f", "g", PeerStatus.Streaming))
        ))
        val sessionId = SessionId(newSession.id, leechId)
        when(nodeMonitor.isHostAvailable("g")).thenReturn(false)

        handler ! ReceiveStreamRequest(sessionId, seedId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs.status should equal(StreamNotReady.status)
    }
    it should "return error response when channel is not available" in {
        val leechId = idGenerator.generateId
        val seedId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(
            id = null,
            peers = Set(Peer(leechId, "b", "c", "d", null), Peer(seedId, "b", "c", config.getConfiguration.hostId, PeerStatus.Retry))
        ))
        val sessionId = SessionId(newSession.id, leechId)

        handler ! ReceiveStreamRequest(sessionId, seedId).toHttpRequest

        coordinator.expectMsgClass(classOf[LeechConnected])
        coordinator.reply(LeechFailure("Channel not ready"))

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.Accepted)
        Thread.sleep(100)
    }
    it should "return error response when session id is not provided" in {
        handler ! request(ReceiveStreamRequestFactory.Method, ReceiveStreamRequestFactory.Url)

        val rs = expectMsgClass(classOf[HttpResponse])
        assertEmptySessionID(rs)
    }

    it should "return error response when session not found" in {
        handler ! ReceiveStreamRequest(SessionId(idGenerator.generateId, null), idGenerator.generateId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        assertSessionNotFound(rs)
    }
    it should "return error response when channel not found" in {
        val peerId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(id = null))
        val sessionId = SessionId(newSession.id, peerId)

        handler ! ReceiveStreamRequest(sessionId, idGenerator.generateId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.NotFound)
        rs should contain("No such channel")
    }
    it should "return error response when seed id is not provided" in {
        val peerId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(id = null))
        val sessionId = SessionId(newSession.id, peerId)

        handler ! request(ReceiveStreamRequestFactory.Method, ReceiveStreamRequestFactory.Url + "/" + sessionId)

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.BadRequest)
        rs should contain("Seed ID not provided")
        verify(repository, never).get(anyString)
    }

    "/close" should "remove session when no more peers and originator's request" in {
        val seedId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(
            id = null,
            originator = seedId,
            peers = Set(Peer(seedId, "b", "c", config.getConfiguration.hostId, PeerStatus.Streaming))
        ))
        val sessionId = SessionId(newSession.id, seedId)

        handler ! CloseRequest(sessionId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.OK)
        verify(repository).remove(newSession.id)
    }
    it should "remove peer from session when it's not an originator" in {
        val leechId = idGenerator.generateId
        val seedId = idGenerator.generateId
        val seedPeer = Peer(seedId, "b", "c", config.getConfiguration.hostId, PeerStatus.Retry)
        val newSession = repository.put(dummySession.copy(
            id = null,
            originator = seedId,
            peers = Set(Peer(leechId, "b", "c", "d", null), seedPeer)
        ))
        val sessionId = SessionId(newSession.id, leechId)

        handler ! CloseRequest(sessionId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.OK)
        verify(repository, never).remove(newSession.id)
        repository.get(newSession.id).get.peers should contain only seedPeer
    }
    it should "return error response when peer is not in session" in {
        val seedId = idGenerator.generateId
        val leechId = idGenerator.generateId
        val newSession = repository.put(dummySession.copy(
            id = null,
            originator = seedId,
            peers = Set(Peer(seedId, "b", "c", config.getConfiguration.hostId, PeerStatus.Streaming))
        ))
        val sessionId = SessionId(newSession.id, leechId)

        handler ! CloseRequest(sessionId).toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.BadRequest)
        verify(repository, never).remove(newSession.id)
    }

    "ping" should "respond" in {
        handler ! PingRequest.toHttpRequest

        val rs = expectMsgClass(classOf[HttpResponse])
        rs shouldHave status(StatusCodes.NoContent)
    }

    def getSessionId(rs: HttpResponse): SessionId = {
        SessionId(unmarshall(rs.entity) > "session")
    }

    def assertEmptySessionID(rs: HttpResponse) {
        rs shouldHave status(StatusCodes.BadRequest)
        rs should contain("Empty session ID")
        verifyZeroInteractions(repository)
    }

    def assertSessionNotFound(rs: HttpResponse) {
        rs shouldHave status(StatusCodes.NotFound)
    }
}
