package edu.pk.hach.server.fixture

import org.springframework.context.support.ClassPathXmlApplicationContext
import com.typesafe.config.ConfigFactory

object EmbedableHachServer {
    private lazy val ctx = new ClassPathXmlApplicationContext("hach-context.xml")

    def start(interface: String = null, port: Int = -1) {
        if (interface != null)
            System.setProperty("hach.server.interface", interface)
        if (port > 0)
            System.setProperty("hach.server.port", port.toString)

        ConfigFactory.invalidateCaches()
        ctx.start()
    }

    def stop() {
        ctx.stop()
    }
}
