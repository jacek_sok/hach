package edu.pk.hach.server

import org.scalatest._
import akka.testkit.{ImplicitSender, TestKit}
import akka.actor.ActorSystem

abstract class UnitSpec extends TestKit(ActorSystem("hach-test")) with ImplicitSender with FlatSpecLike with Matchers with OptionValues with BeforeAndAfter