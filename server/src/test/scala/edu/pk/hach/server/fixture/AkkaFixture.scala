package edu.pk.hach.server.fixture

import akka.testkit.TestActorRef
import akka.actor.{ActorSystem, Actor}
import org.slf4j.LoggerFactory

trait AkkaFixture {
    implicit val system: ActorSystem

    def createTestActor = TestActorRef(new Actor {
        override def receive = {
            case o: AnyRef => LoggerFactory.getLogger(self.toString()).info("Receiving message: {}", o)
        }
    })
}
