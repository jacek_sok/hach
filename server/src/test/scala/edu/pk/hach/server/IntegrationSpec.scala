package edu.pk.hach.server

import org.scalatest._
import org.mockito.Mockito._
import org.scalatest.Matchers._
import edu.pk.hach.server.fixture.HttpFixture
import spray.http._
import spray.http.StatusCodes._
import akka.actor.{ActorRef, ActorSystem}
import akka.util.Timeout
import org.scalatest.time.{Millis, Seconds, Span}
import akka.io.IO
import akka.pattern.ask
import akka.actor.ActorDSL._
import spray.can.Http
import edu.pk.hach.server.session.{SessionId, SessionRepository}
import org.springframework.test.context.{TestContextManager, ContextConfiguration}
import org.springframework.beans.factory.annotation.Autowired
import edu.pk.hach.protocol.EntityMarshaller._
import edu.pk.hach.protocol.model._
import spray.http.HttpRequest
import edu.pk.hach.protocol.model.StatusRequest
import spray.http.HttpResponse
import edu.pk.hach.protocol.model.SendStreamRequest
import scala.collection.mutable.ListBuffer
import edu.pk.hach.protocol.{Request, HttpConverters}
import com.typesafe.config.ConfigFactory
import java.util.concurrent.TimeUnit

@ContextConfiguration(locations = Array("classpath:hach-context.xml", "classpath:hach-context-test.xml"))
class IntegrationSpec extends FeatureSpec with HttpFixture with BeforeAndAfterAll {
    System.setProperty("hach.server.interface", hostHeader.host)
    System.setProperty("hach.server.port", hostHeader.port.toString)
    ConfigFactory.invalidateCaches()

    implicit lazy val actorSystem = ActorSystem()
    implicit lazy val timeout = Timeout(3000, TimeUnit.MILLISECONDS)
    implicit lazy override val patienceConfig = PatienceConfig(timeout = Span(3, Seconds), interval = Span(20, Millis))
    @Autowired val repository: SessionRepository = null
    lazy val CtxManager = new TestContextManager(this.getClass)

    override protected def beforeAll() {
        CtxManager.prepareTestInstance(this)
    }

    override protected def afterAll() {
        CtxManager.afterTestClass()
        actorSystem.shutdown()
    }

    implicit def stringArray2Chunks(s: String): Array[Byte] = {
        s.getBytes
    }

    feature("Session status") {
        import edu.pk.hach.protocol.model.StatusRequestFactory._
        scenario("User sends request without session ID") {
            val rq = request(Method, Url)

            val rs = sendReceive(rq)

            rs shouldHave status(BadRequest)
            rs.entity.asString should include("Empty session ID")
        }

        scenario("Repository throws exception") {
            val sessionId = "123"
            val rq = StatusRequest(sessionId)
            doThrow(new RuntimeException("Ignore It")).when(repository).get(sessionId)

            val rs = sendReceive(rq)

            rs shouldHave status(InternalServerError)
            rs should contain("Debug:\n")
        }
    }

    feature("Streaming") {

        ignore("Complete streaming between two users") {
            val session = unmarshall(sendReceive(InitRequest()).entity)
            val seedSessionId: String = session > "session"
            sendReceive(InitPeerRequest(seedSessionId, "Alice"))
            val peerSessionId: String = unmarshall(sendReceive(InitPeerRequest(session > "exchange", "Bob")).entity) > "session"

            val (seedRs, leechRs, chunks) = sendReceive(seedSessionId, peerSessionId, "J", "A", "C", "E", "K")

            seedRs shouldHave status(OK)
            seedRs shouldHave parameters("status" -> "Opened")
            leechRs shouldHave status(OK)
            chunks.tail should be(Array("J", "A", "C", "E", "K"))
        }

        scenario("Stream not ready") {
            val session = unmarshall(sendReceive(InitRequest()).entity)
            val seedSessionId: String = session > "session"
            sendReceive(InitPeerRequest(seedSessionId, "Alice"))
            val peerSessionId: String = unmarshall(sendReceive(InitPeerRequest(session > "exchange", "Bob")).entity) > "session"

            val rs = sendReceive(ReceiveStreamRequest(peerSessionId, SessionId(seedSessionId).peer))

            rs shouldHave status(Accepted)
        }
    }

    def sendReceive(rq: HttpRequest): HttpResponse = (IO(Http) ? request(rq)).mapTo[HttpResponse].futureValue

    def sendReceive(rq: Request): HttpResponse = sendReceive(rq.toHttpRequest)

    def sendReceive(seed: String, leech: String, chunks: Array[Byte]*) = {
        import Thread.sleep
        sleep(100)
        var receiverResponse, senderResponse: HttpResponse = null
        val transmittedChunks = ListBuffer[Array[Byte]]()
        val receiverRef = actor("http-client-receiver")(new Act with HttpConverters {
            var senderRef: ActorRef = _
            var response: HttpResponse = _
            become {
                case "Connect" =>
                    senderRef = sender
                    IO(Http) ! Http.Connect("localhost", port = 10123)

                case _: Http.Connected =>
                    sender ~> ReceiveStreamRequest(leech, SessionId(seed).peer)
                    sleep(100)
                    senderRef ! "Continue"

                case MessageChunk(data, _) =>
                    transmittedChunks += data.toByteArray
                    sleep(50)

                case ChunkedResponseStart(x) =>
                    response = x
                    sleep(50)

                case ChunkedMessageEnd =>
                    receiverResponse = response
                    sleep(50)
            }
        })

        val rs = actor("http-client-sender")(new Act {
            var connection: ActorRef = _
            var originator: ActorRef = _
            become {
                case "Connect" =>
                    IO(Http) ! Http.Connect("localhost", port = 10123)
                    originator = sender

                case _: Http.Connected =>
                    sender ! SendStreamRequest(seed).toHttpRequest
                    connection = sender
                    receiverRef ! "Connect"
                    sleep(50)

                case "Continue" =>
                    sleep(800)
                    chunks.foreach(chunk => {
                        connection ! MessageChunk(chunk)
                        sleep(50)
                    })
                    connection ! ChunkedMessageEnd()

                case rs: HttpResponse =>
                    senderResponse = rs
                    sleep(30)
                    originator ! "Done"
            }
        }) ? "Connect"

        rs.futureValue
        (1 to 10) takeWhile (x => {
            sleep(100)
            receiverResponse == null
        })
        (senderResponse, receiverResponse, transmittedChunks.toArray.map(new String(_)))
    }
}
