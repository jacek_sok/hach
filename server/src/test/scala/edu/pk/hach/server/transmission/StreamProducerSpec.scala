package edu.pk.hach.server.transmission

import edu.pk.hach.server.UnitSpec
import akka.testkit.{TestProbe, TestActorRef}
import org.mockito.Mockito._
import edu.pk.hach.server.session.{Session, Peer, SessionId, SessionRepository}
import edu.pk.hach.protocol.{SessionStatus, PeerStatus}
import spray.http.{MessageChunk, HttpResponse, ChunkedMessageEnd}
import edu.pk.hach.server.fixture.AkkaFixture
import java.util.concurrent.ArrayBlockingQueue
import edu.pk.hach.server.cluster.NodeMonitor

class StreamProducerSpec extends UnitSpec with AkkaFixture {
    val repository = mock(classOf[SessionRepository])
    val nodeMonitor = mock(classOf[NodeMonitor])

    before {
        reset(repository)
    }

    "StreamProducer" should "create and terminate properly" in {
        val sessionId = SessionId("1", "a")
        val peer = Peer("a", "me", "localhost", "127.0.0.1", PeerStatus.Ready)
        when(repository.get(sessionId.uuid)).thenReturn(Some(Session(sessionId, "me", Set(peer), SessionStatus.Opened)))

        val producer = TestActorRef(new StreamProducer(repository, nodeMonitor, sessionId, peer))

        producer.underlyingActor.queues should be(empty)

        producer ! ChunkedMessageEnd

        verify(repository).put(sessionId, peer.copy(status = PeerStatus.Closed, streamHost = null))
        expectMsgClass(classOf[HttpResponse])
    }
    it should "add new leech and watch it" in {
        val sessionId = SessionId("1", "a")
        val peer = Peer("a", "me", "localhost", "127.0.0.1", PeerStatus.Ready)
        val producer = TestActorRef(new StreamProducer(repository, nodeMonitor, sessionId, peer))
        val leech = createTestActor

        assume(producer.underlyingActor.queues.isEmpty)

        producer !(leech, new ArrayBlockingQueue[MessageChunk](1))

        producer.underlyingActor.queues should have size 1

        leech.stop()

        producer.underlyingActor.queues should be(empty)
    }
    it should "send chunk to all leeches" in {
        val sessionId = SessionId("1", "a")
        val peer = Peer("a", "me", "localhost", "127.0.0.1", PeerStatus.Ready)
        val producer = TestActorRef(new StreamProducer(repository, nodeMonitor, sessionId, peer))

        val queue1 = new ArrayBlockingQueue[MessageChunk](2)
        val queue2 = new ArrayBlockingQueue[MessageChunk](2)

        producer ! (TestProbe().ref, queue1)
        producer ! (TestProbe().ref, queue2)

        producer.underlyingActor.queues.size should be(2)

        producer ! MessageChunk("J")
        producer ! MessageChunk("A")

        queue1 should contain allOf(MessageChunk("J"), MessageChunk("A"))
        queue2 should contain allOf(MessageChunk("J"), MessageChunk("A"))
    }
    it should "remove overflow from queue when no space left for chunk" in {
        val sessionId = SessionId("1", "a")
        val peer = Peer("a", "me", "localhost", "127.0.0.1", PeerStatus.Ready)
        val producer = TestActorRef(new StreamProducer(repository, nodeMonitor, sessionId, peer))

        val queue = new ArrayBlockingQueue[MessageChunk](2)

        producer ! (TestProbe().ref, queue)

        producer ! MessageChunk("J")
        producer ! MessageChunk("A")
        producer ! MessageChunk("C")
        producer ! MessageChunk("E")
        producer ! MessageChunk("K")

        queue should contain allOf(MessageChunk("E"), MessageChunk("K"))
    }
}
