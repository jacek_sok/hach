package edu.pk.hach.server.session.impl

import edu.pk.hach.server.UnitSpec

class DefaultIdGeneratorSpec extends UnitSpec {
    "Generator" should "generate different, not null IDs" in {
        val generator = new DefaultIdGenerator

        val ids = for (i <- 1 to 10000) yield generator.generateId

        ids should not contain null.asInstanceOf[String]
        ids should not contain ""
        ids should equal(ids.distinct)
    }
}
