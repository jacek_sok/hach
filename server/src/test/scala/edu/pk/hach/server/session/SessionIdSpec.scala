package edu.pk.hach.server.session

import edu.pk.hach.server.UnitSpec

class SessionIdSpec extends UnitSpec {
    "SessionId" should "be converted to string" in {
        val id = SessionId("1", "a")

        val s: String = id

        s should not be empty
        s should not contain "1|a"
    }

    "String" should "be converted to SessionId" in {
        val id = SessionId("1", "a")

        val result = SessionId(id.toString)

        result should equal(id)
    }
    it should "Fail to convert when invalid string session id" in {
        intercept[InvalidSessionIdException] {
            SessionId("invalid id")
        }
    }

    "Converter" should "create same responses for the same input" in {
        val id1 = SessionId("1", "a")
        val id2 = SessionId("1", "a")

        val r1 = id1.toString
        val r2 = id2.toString

        r1 should equal(r2)
    }
}
