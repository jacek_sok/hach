name := "HACH :: Stream Server"

libraryDependencies += "org.springframework" % "spring-core" % SpringVersion

libraryDependencies += "org.springframework" % "spring-test" % SpringVersion

libraryDependencies += "org.springframework" % "spring-context" % SpringVersion

libraryDependencies += "org.apache.zookeeper" % "zookeeper" % "3.4.6" excludeAll(
    ExclusionRule("log4j", "log4j"),
    ExclusionRule("org.slf4j", "slf4j-log4j12")
)

libraryDependencies += "com.basho.riak" % "riak-client" % "1.4.4"

libraryDependencies += "io.spray" % "spray-can" % SprayVersion

libraryDependencies += "org.jasypt" % "jasypt" % "1.9.2"

libraryDependencies += "io.spray" % "spray-client" % SprayVersion % "test"

libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % AkkaVersion % "test"

excludeFilter in unmanagedResources := "logback*.xml"

val assembleServer = taskKey[Unit]("copies configuration files to the pack directory")

packSettings

packMain := Map(
    "hach-server" -> "edu.pk.hach.server.Bootstrap",
    "riak-admin" -> "edu.pk.hach.server.session.impl.AdminTool"
)

packJvmOpts := Map(
    "hach-server" -> Seq("-Dcom.sun.management.jmxremote",
                         "-Dcom.sun.management.jmxremote.port=8010",
                         "-Dcom.sun.management.jmxremote.rmi.port=8011",
                         "-Dcom.sun.management.jmxremote.local.only=false",
                         "-Dcom.sun.management.jmxremote.authenticate=false",
                         "-Dcom.sun.management.jmxremote.ssl=false")
)

packExtraClasspath := Map("hach-server" -> Seq("${PROG_HOME}/conf"))

packGenerateWindowsBatFile := false

assembleServer := {
    val confDir = pack.value / "conf"
    streams.value.log("Copying configuration files to: " + confDir)
    IO.copyDirectory((sourceDirectory in Compile).value / "resources", confDir)
}

