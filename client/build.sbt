name := "HACH :: Client"

resolvers += "xuggle repo" at "http://xuggle.googlecode.com/svn/trunk/repo/share/java/"

libraryDependencies += "io.spray" % "spray-client" % SprayVersion exclude("joda-time", "joda-time")

libraryDependencies += "joda-time" % "joda-time" % "2.3"

libraryDependencies += "org.joda" % "joda-convert" % "1.2"

libraryDependencies += "org.scala-lang" % "scala-swing" % scalaVersion.value

libraryDependencies += "xuggle" % "xuggle-xuggler" % "5.4"

libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % AkkaVersion

libraryDependencies += "com.typesafe.akka" %% "akka-testkit" % AkkaVersion % "test"

// for benchmark only
libraryDependencies += "org.apache.zookeeper" % "zookeeper" % "3.4.6" excludeAll(
    ExclusionRule("log4j", "log4j"),
    ExclusionRule("org.slf4j", "slf4j-log4j12")
)

val assembleClient = taskKey[Unit]("copies configuration files to the pack directory")

packSettings

packMain := Map(
    "hach-client" -> "edu.pk.hach.client.Bootstrap",
    "benchmark" -> "edu.pk.hach.client.Benchmark"
)

packJvmOpts := Map("benchmark" -> Seq("-Xms256m", "-Xmx1048m", "-Xss228k"))

packExtraClasspath := Map("hach-client" -> Seq("${PROG_HOME}/conf"))

packGenerateWindowsBatFile := false

assembleClient := {
    val confDir = pack.value / "conf"
    streams.value.log("Copying configuration files to: " + confDir)
    IO.copyDirectory((sourceDirectory in Compile).value / "resources", confDir)
}
