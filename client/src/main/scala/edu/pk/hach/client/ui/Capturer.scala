package edu.pk.hach.client.ui

import akka.actor.{Terminated, ActorRef, Actor}
import com.xuggle.xuggler._
import com.xuggle.mediatool.{MediaListenerAdapter, ToolFactory, IMediaReader}
import com.xuggle.mediatool.event.{ICloseEvent, IVideoPictureEvent}
import com.xuggle.xuggler.IStreamCoder.Direction
import java.awt.image.BufferedImage
import com.xuggle.xuggler.video.ConverterFactory
import javax.sound.sampled._
import com.xuggle.xuggler.IAudioSamples.Format
import edu.pk.hach.client.Stream.{Empty, Listen, VideoSlice, AudioSlice}
import edu.pk.hach.client.Config
import scala.collection.mutable.ArrayBuffer
;

class Capturer(config: Config, player: ActorRef) extends Actor {
    val container = IContainer.make()
    var camReader = {
        val format = IContainerFormat.make()
        format.setInputFormat(config.video.driver)
        val params = IMetaData.make()
        params.setValue("framerate", "25/1")
        params.setValue("video_size", "320x240")
        container.open(config.video.device, IContainer.Type.READ, format, false, true, params, null)
        val camReader = ToolFactory.makeReader(container)
        camReader.setBufferedImageTypeToGenerate(BufferedImage.TYPE_3BYTE_BGR)
        camReader
    }
    val audioReader = new AudioGrabber
    var stop: Boolean = _

    override def postStop() {
        stop = true
        audioReader.close()
        camReader.close()
        container.close()
    }

    override def receive = {
        case Listen(actor) =>
            camReader.addListener(new CamListener(actor))
            new Thread(new StreamReader(camReader)).start()
            new Thread(new AudioSender(actor, audioReader)).start()
            context.watch(actor)

        case Terminated(_) | "Stop" =>
            context.stop(self)
    }

    class CamListener(actor: ActorRef) extends MediaListenerAdapter {
        val cfg = config.video
        val streamCoder = IStreamCoder.make(Direction.ENCODING, cfg.codec)
        val converter = ConverterFactory.createConverter(ConverterFactory.XUGGLER_BGR_24, cfg.pixelFormat, cfg.size.width, cfg.size.height)
        val packet = IPacket.make()
        init()

        def init() {
            streamCoder.setNumPicturesInGroupOfPictures(cfg.fps)
            streamCoder.setBitRate(200000)
            streamCoder.setBitRateTolerance(10000)
            streamCoder.setPixelType(cfg.pixelFormat)
            streamCoder.setWidth(cfg.size.width)
            streamCoder.setHeight(cfg.size.height)
            streamCoder.setFlag(IStreamCoder.Flags.FLAG_QSCALE, true)
            streamCoder.setGlobalQuality(0)

            val rate = IRational.make(cfg.fps, 1)
            streamCoder.setFrameRate(rate)
            streamCoder.setTimeBase(IRational.make(rate.getDenominator, rate.getNumerator))

            val codecOptions = IMetaData.make()
            codecOptions.setValue("tune", "zerolatency")

            streamCoder.open(codecOptions, null)
        }

        override def onVideoPicture(event: IVideoPictureEvent) {
            val image = event.getImage
            val picture = converter.toPicture(image, event.getPicture.getTimeStamp)

            try streamCoder.encodeVideo(packet, picture, 0) finally picture.delete()

            if (packet.isComplete) {
                try {
                    val size = packet.getSize
                    val buffer = new Array[Byte](size)
                    packet.get(0, buffer, 0, size)
                    actor ! VideoSlice(buffer)
                    player ! VideoSlice(buffer)
                } finally packet.reset()
            }
        }

        override def onClose(event: ICloseEvent) {
            actor ! Empty
            streamCoder.close()
        }
    }

    class AudioGrabber {
        val cfg = config.audio
        val coder = IStreamCoder.make(Direction.ENCODING, cfg.codec)
        var audioTime = 0L
        val line = {
            coder.setSampleRate(cfg.sampleRate)
            coder.setChannels(cfg.channels)
            coder.setFrameRate(IRational.make(cfg.sampleRate, 1))
            coder.setSampleFormat(Format.FMT_S16)
            coder.open(IMetaData.make(), IMetaData.make())

            val audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, cfg.sampleRate, 16, cfg.channels, cfg.frameSize, cfg.sampleRate, false)
            val dataLineInfo = new DataLine.Info(classOf[TargetDataLine], audioFormat)
            val line = AudioSystem.getLine(dataLineInfo).asInstanceOf[TargetDataLine]

            line.open()
            line.start()

            line
        }

        def close() {
            line.stop()
            line.close()
            coder.close()
        }

        def grab() = {
            val bufferSize = 4096
            val buffer = new Array[Byte](bufferSize)
            val bytes = line.read(buffer, 0, bufferSize)

            val samples = IAudioSamples.make(1024, coder.getChannels)
            samples.put(buffer, 0, 0, bytes)
            samples.setComplete(true, bytes / cfg.frameSize, coder.getSampleRate, coder.getChannels, IAudioSamples.Format.FMT_S16, audioTime / 4)
            audioTime += bytes

            val result = ArrayBuffer[Byte]()
            var consumed = 0
            while (consumed < samples.getNumSamples) {
                val packet = IPacket.make()
                consumed += coder.encodeAudio(packet, samples, consumed)
                if (packet.isComplete) {
                    val size = packet.getSize
                    val buffer = new Array[Byte](size)
                    packet.get(0, buffer, 0, size)
                    result ++= buffer
                }
                packet.delete()
            }
            samples.delete()
            result.toArray
        }
    }

    class AudioSender(actor: ActorRef, audio: AudioGrabber) extends Runnable {
        override def run() {
            while (!stop) {
                val bytes = audio.grab()
                actor ! AudioSlice(bytes)
                player ! AudioSlice(bytes)
            }
            actor ! Empty
        }
    }

    class StreamReader(reader: IMediaReader) extends Runnable {
        override def run() {
            while (!stop) {
                reader.readPacket()
                Thread.sleep(20)
            }
        }
    }

}
