package edu.pk.hach.client

import akka.actor.{Terminated, ActorRef, Actor}
import akka.io.IO
import spray.can.Http
import spray.http.{ChunkedMessageEnd, MessageChunk}
import edu.pk.hach.client.event.ProtocolListener
import edu.pk.hach.client.Stream._
import spray.http.HttpResponse
import edu.pk.hach.protocol.model.{RedirectToNodeResponseFactory, SendStreamRequest}
import edu.pk.hach.client.Stream.Listen
import java.nio.ByteBuffer
import scala.collection.mutable.ListBuffer
import edu.pk.hach.protocol.meta.Server
import org.slf4j.LoggerFactory
import akka.event.Logging

class StreamProducer(session: String,
                     feeder: ActorRef,
                     connector: Connector,
                     listener: ProtocolListener) extends Actor with StreamMetric {
    private val Log = Logging(context.system, this)
    protected val id = "producer"
    implicit val actorSystem = Bootstrap.actorSystem
    private val audioBuffer = ListBuffer[AudioSlice]()

    override def receive: Receive = initialize(connector.getServer, notify = true)

    def initialize(server: Server, notify: Boolean): Receive = {
        case "Start" =>
            Log.info("Connecting to {}", server)
            IO(Http) ! Http.Connect(server.host, server.port)

        case _: Http.Connected =>
            Log.debug("Connection established")
            sender ! SendStreamRequest(session).toHttpRequest
            if (notify) feeder ! Listen(self)
            val http = sender()
            context.watch(http)
            context.unbecome()
            context.become(pipeline(http, server))

        case s: StreamSlice =>
            Log.debug("Dropping stream slice... {}", s)
    }

    def pipeline(http: ActorRef, server: Server): Receive = {
        case v: VideoSlice =>
            val chunk = audioBuffer.foldLeft(slice2stream(v))((result, slice) => result ++ slice2stream(slice))
            audioBuffer.clear()
            if (!chunk.isEmpty) {
                http ! MessageChunk(chunk)
                updateMetric(chunk)
            }

        case a: AudioSlice =>
            audioBuffer += a

        case Empty =>
            http ! ChunkedMessageEnd()

        case RedirectToNodeResponseFactory(location) =>
            Log.info("Stream redirected to: {}", location)
            self ! "Start"
            context.become(initialize(location, notify = false))

        case rs: HttpResponse =>
            printMetric()
            Log.debug("Send Stream RS: {}", rs)
            context.stop(self)

        case Terminated(_) =>
            printMetric()
            Log.warning("Connection closed from the other end, re-establishing")
            connector.markDown(server)
            self ! "Start"
            listener.onStreamSendError(session, "Connection terminated")
            context.unbecome()
            context.become(initialize(connector.getServer, notify = false))
    }

    def slice2stream(slice: StreamSlice): Array[Byte] = ByteBuffer.allocate(4).putInt(slice.stream.size).array() ++ slice.stream
}