package edu.pk.hach.client

import akka.actor._
import akka.io.IO
import spray.can.Http
import spray.http.HttpResponse
import spray.http.HttpHeaders.Host
import edu.pk.hach.protocol.model._
import edu.pk.hach.protocol.{Request, HttpConverters}
import edu.pk.hach.client.event.ProtocolListener
import org.slf4j.LoggerFactory
import edu.pk.hach.protocol.model.StatusRequest
import edu.pk.hach.client.Stream.{PeerNotAvailable, server2host}
import edu.pk.hach.protocol.model.CloseRequest
import edu.pk.hach.protocol.model.InitPeerRequest
import edu.pk.hach.protocol.model.InitRequest
import scala.Some
import scala.concurrent.duration._
import scala.util.Failure
import scala.util.Success
import spray.can.Http.ConnectionException
import edu.pk.hach.protocol.meta.Server
import java.util.UUID

class Client(config: Config, listener: ProtocolListener)(implicit actorSystem: ActorSystem) {

    import Client._

    class Coordinator extends Actor with FSM[State, Data] with HttpConverters {

        import actorSystem.dispatcher

        implicit class CustomAsyncRequestSender(sender: ActorRef)(implicit host: Host = null) extends AsyncRequestSender(sender) {
            override def ~>(rq: Request) = {
                lastRequest = rq
                super.~>(rq)
            }
        }

        private var lastRequest: Request = _
        private var lastServer: Server = _

        private val connector = new Connector(config.servers)
        private var peerName: String = _
        private var sessionReadTask: Cancellable = _

        private implicit val timeout = config.timeout
        private implicit def host: Host ={
            lastServer = connector.getServer
            lastServer
        }

        startWith(Idle, NA)

        when(Idle) {
            case Event(Start(peer, None), _) =>
                peerName = peer
                listener.onCreatingSession(peerName)
                IO(Http) ~> InitRequest()
                goto(InitializingSession)

            case Event(Start(peer, Some(session)), _) =>
                peerName = peer
                initializePeer(session)
                goto(InitializingPeer) using Session(session)
        }

        when(InitializingSession) {
            case Event(InitResponseFactory(session, exchange), _) =>
                listener.onSessionCreated(session, exchange)
                initializePeer(session)
                goto(InitializingPeer) using Session(session)
        }

        when(InitializingPeer) {
            case Event(InitPeerResponseFactory(session), _) =>
                listener.onAttachedToSession(session)
                readSession(session)
                goto(ReadingSession) using Session(session)
        }

        when(ReadingSession) {
            case Event(StatusResponseFactory(status, peers, server), session: Session) =>
                listener.onSessionRead(session.id, status, peers, server)
                sessionReadTask = actorSystem.scheduler.schedule(2 seconds, 2 second, self, Reload(session.id))
                goto(Ready) using Peers(session.id, peers)
        }

        when(Ready) {
            case Event(SendStream(producer), peersInfo@Peers(session, peers)) =>
                val props = Props.create(classOf[StreamProducer], session, producer, connector, listener).withDispatcher("stream-dispatcher")
                actorSystem.actorOf(props) ! "Start"
                stay() using peersInfo

            case Event(ReceiveStream(peerId, consumer), peersInfo@Peers(session, peers)) =>
                peers.find(_.id == peerId) match {
                    case Some(peer) =>
                        val peerToPass = if (peer.host != null && connector.isDown(Server(peer.host))) peer.copy(host = null) else peer
                        val props = Props.create(classOf[StreamConsumer], session, peerToPass, consumer, connector, listener).withDispatcher("stream-dispatcher")
                        actorSystem.actorOf(props) ! "Start"
                    case _ =>
                        consumer ! PeerNotAvailable(peerId)
                }
                stay() using peersInfo
        }

        whenUnhandled {
            case Event(Stop, _) =>
                goto(Idle) using NA

            case Event(Reload(session), _) =>
                readSession(session)
                stay()

            case Event(StatusResponseFactory(status, peers, server), Peers(session, _)) if state != Idle =>
                listener.onSessionRead(session, status, peers, server)
                stay() using Peers(session, peers)

            case Event(SessionNotFoundFactory(_), _) =>
                listener.onSessionNotFound()
                goto(Idle) using NA

            case Event(Status.Failure(e: ConnectionException), _) =>
                try {
                    connector.markDown(lastServer)
                    IO(Http) ~> lastRequest
                    stay()
                } catch {
                    case e: IllegalStateException =>
                        listener.onConnectionError(e.toString)
                        goto(Idle) using NA
                }

            case Event(e, d) =>
                Log.warn("Unhandled event: state={}, event={}, data={}", Array(stateName, e, d).asInstanceOf[Array[AnyRef]])
                stay()
        }

        onTransition {
            case Idle -> InitializingSession =>
                Log.info("Initializing session")
            case _ -> InitializingPeer =>
                Log.info("Initializing peer: " + peerName)
            case InitializingPeer -> ReadingSession =>
                Log.info("Reading session")
            case ReadingSession -> Ready =>
                Log.info("Client ready for streaming")
            case Ready -> Idle =>
                closeSession(stateData.asInstanceOf[Peers].session)
            case _ -> Idle if stateData.isInstanceOf[Session] =>
                closeSession(stateData.asInstanceOf[Session].id)
        }

        onTransition {
            case t => currentState = t._2
        }

        initialize()

        def initializePeer(session: String) {
            listener.onAttachingToSession(session, peerName)
            IO(Http) ~> InitPeerRequest(session, peerName)
        }

        def readSession(session: String) {
            listener.onReadingSession(session)
            IO(Http) ~> StatusRequest(session)
        }

        def closeSession(session: String) {
            Log.info("Closing connection")
            if (sessionReadTask != null) sessionReadTask.cancel()
            (IO(Http) <~> CloseRequest(session)).mapTo[HttpResponse].onComplete {
                case Success(rs) => Log.debug(rs.toString)
                case Failure(ex) => Log.error("Failed to close session", ex)
            }
        }
    }

    private val coordinator = actorSystem.actorOf(Props(classOf[Coordinator], this))
    private var currentState: State = Idle

    def start(peerName: String, sessionId: String = null) {
        coordinator ! Start(peerName, if (sessionId == null) None else Some(sessionId))
    }

    def send(producer: ActorRef) {
        coordinator ! SendStream(producer)
    }

    def receive(peer: String, consumer: ActorRef) {
        coordinator ! ReceiveStream(peer, consumer)
    }

    def stop() {
        coordinator ! Stop
    }

    def state = currentState

    lazy val uuid = UUID.randomUUID().toString
}

object Client {
    val Log = LoggerFactory.getLogger(classOf[Client])

    // states

    sealed trait State

    case object Idle extends State

    case object InitializingSession extends State

    case object InitializingPeer extends State

    case object ReadingSession extends State

    case object Ready extends State

    // data

    sealed trait Data

    case object NA extends Data

    case class Session(id: String) extends Data

    case class Peers(session: String, peers: Set[PeerInfo]) extends Data

    case class Stream(session: String, producer: ActorRef, consumers: Set[ActorRef]) extends Data

    // actions

    case class Start(peerName: String, session: Option[String])

    case class SendStream(producer: ActorRef)

    case class ReceiveStream(peer: String, consumer: ActorRef)

    case object Stop

    case class Reload(session: String)

}
