package edu.pk.hach.client

import akka.util.Timeout
import java.awt.Dimension
import com.xuggle.xuggler.{ICodec, IPixelFormat}
import edu.pk.hach.protocol.meta.Server

case class Config(servers: List[Server],
                  timeout: Timeout,
                  video: VideoConfig,
                  audio: AudioConfig)

case class AudioConfig(codec: ICodec.ID,
                       sampleRate: Int,
                       channels: Int,
                       frameSize: Int)

case class VideoConfig(driver: String,
                       device: String,
                       size: Dimension,
                       codec: ICodec.ID,
                       pixelFormat: IPixelFormat.Type,
                       fps: Int)

