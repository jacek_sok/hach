package edu.pk.hach.client

import org.slf4j.{LoggerFactory, Logger}

trait StreamMetric {
    protected val id: String
    lazy private val Log: Logger = LoggerFactory.getLogger("StreamMetric." + id)
    protected var total = 0.0
    protected var averageThroughput = 0.0
    private var lastDataTime = 0L
    private var testData = 0.0

    def updateMetric(data: Array[Byte]) {
        val kb = data.length / 1024.0
        total += kb
        testData += kb
        val t = System.currentTimeMillis - lastDataTime
        if (t >= 1000) {
            val d = testData / (t / 1000.0)
            Log.debug("Throughput: {} kb/s", format(d))

            if (averageThroughput == 0.0) averageThroughput = d
            else averageThroughput = (averageThroughput + d) / 2

            lastDataTime = System.currentTimeMillis()
            testData = 0
        }
    }

    def printMetric() {
        Log.info("Total stream size: {} kb", format(total))
        Log.info("Average throughput: {} kb/s", format(averageThroughput))
    }

    def format(kb: Double) = "%.2f".format(kb)
}
