package edu.pk.hach.client.event

import edu.pk.hach.protocol.SessionStatus.SessionStatusType
import edu.pk.hach.protocol.model.PeerInfo

trait ProtocolListener {
    def onConnectionError(reason: String)
    def onSessionNotFound()
    def onAttachingToSession(session: String, peerName: String)
    def onCreatingSession(peerName: String)
    def onSessionCreated(session: String, exchange: String)
    def onAttachedToSession(session: String)
    def onReadingSession(session: String)
    def onSessionRead(session: String, status: SessionStatusType, peers: Set[PeerInfo], server: String)
    def onStreamClosed(session: String, peer: String)
    def onStreamReceiveError(session: String, peer: String, error: String)
    def onStreamSendError(session: String, error: String)
}
