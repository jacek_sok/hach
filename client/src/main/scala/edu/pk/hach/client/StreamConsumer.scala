package edu.pk.hach.client

import akka.actor.{Terminated, Actor, ActorRef}
import akka.io.{Tcp, IO}
import akka.util.Timeout
import spray.can.Http
import edu.pk.hach.client.event.ProtocolListener
import edu.pk.hach.protocol.HttpConverters
import edu.pk.hach.protocol.model.{PeerInfo, StreamNotReady, RedirectToNodeResponseFactory, ReceiveStreamRequest}
import edu.pk.hach.client.Stream.{AudioSlice, VideoSlice, Empty, server2host}
import spray.http.{HttpResponse, ChunkedMessageEnd, MessageChunk}
import scala.concurrent.duration.DurationLong
import java.nio.ByteBuffer
import spray.http.HttpHeaders.Host
import edu.pk.hach.protocol.meta.Server
import akka.actor.Status.Failure
import akka.event.Logging

class StreamConsumer(session: String,
                     peer: PeerInfo,
                     receiver: ActorRef,
                     connector: Connector,
                     listener: ProtocolListener) extends Actor with HttpConverters with StreamMetric {
    protected val id = peer.name
    implicit var hostHeader: Host = if (peer.host != null) Server(peer.host) else connector.getServer
    implicit val actorSystem = Bootstrap.actorSystem
    private val Log = Logging(context.system, this)

    implicit val timeout: Timeout = Timeout(5 seconds)

    override def receive: Receive = initialize

    def initialize: Receive = {
        case "Start" =>
            Log.debug("Trying to receive stream for peer '{}' from: {}", peer, hostHeader)
            IO(Http) ~> ReceiveStreamRequest(session, peer.id)
            context.watch(receiver)

        case RedirectToNodeResponseFactory(location) =>
            Log.info("Stream redirected to {}", location)
            if (!connector.isDown(location))
                hostHeader = location
            else {
                Log.info("Ignoring - host is down")
                Thread.sleep(500)
                hostHeader = connector.getServer
            }
            IO(Http) ~> ReceiveStreamRequest(session, peer.id)

        case rs: HttpResponse if rs.status == StreamNotReady.status =>
            Log.debug("Stream not ready for {}", peer)
            Thread.sleep(300)
            self ! "Start"

        case MessageChunk(data, _) =>
            Log.info("Receiving stream for peer '{}' from: {}", peer, hostHeader)
            // first chunk contains only timestamp
            Log.debug("Received timestamp: {}", new String(data.toByteArray))
            context.unbecome()
            context.become(pipeline(sender(), Server(hostHeader.host, hostHeader.port)))
    }

    def pipeline(endpoint: ActorRef, server: Server): Receive = {
        case MessageChunk(data, _) =>
            val bytes = data.toByteArray
            val extractor = new Extractor(bytes)
            updateMetric(bytes)
            receiver ! VideoSlice(extractor.nextSlice)
            extractor.foreach(receiver ! AudioSlice(_))

        case ChunkedMessageEnd(_, _) =>
            receiver ! Empty
            printMetric()
            Log.info("Stream end for {}", peer)
            listener.onStreamClosed(session, peer.id)
            context.stop(self)

        case Tcp.Close | Failure(_) =>
            Log.warning("Stream closed for peer {}", peer)
            receiver ! Empty
            connector.markDown(server)
            listener.onStreamClosed(session, peer.id)

        case Terminated(actor) if actor.eq(receiver) =>
            printMetric()
            receiver ! Empty
            endpoint ! Http.Close
            context.stop(self)

        case x => println("RECEIVED: " + x)
    }

    class Extractor(stream: Array[Byte]) extends Traversable[Array[Byte]] {
        private var offset = 0

        def nextSlice = {
            val len = ByteBuffer.wrap(stream.slice(offset, offset + 4)).getInt
            val slice = stream.slice(offset + 4, offset + len + 4)
            offset += 4 + len
            slice
        }

        override def foreach[U](f: (Array[Byte]) => U): Unit = {
            while (offset < stream.size) {
                f(nextSlice)
            }
        }
    }

}
