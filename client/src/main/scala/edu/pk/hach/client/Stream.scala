package edu.pk.hach.client

import akka.actor.ActorRef
import spray.http.HttpHeaders.Host
import edu.pk.hach.protocol.meta.Server

object Stream {
    type ByteStream = Array[Byte]

    case object Empty

    case class Listen(actor: ActorRef)

    trait StreamSlice {
        val stream: ByteStream
    }

    case class VideoSlice(stream: ByteStream) extends StreamSlice

    case class AudioSlice(stream: ByteStream) extends StreamSlice

    case object CleanUp

    case class PeerNotAvailable(peer: String)

    implicit def server2host(server: Server): Host = Host(server.host, server.port)
}
