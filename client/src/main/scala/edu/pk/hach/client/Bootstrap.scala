package edu.pk.hach.client

import akka.actor.ActorSystem
import scala.swing._
import scala.collection.JavaConversions._
import edu.pk.hach.client.ui.MainWindow
import com.typesafe.config.ConfigFactory
import java.awt.Dimension
import com.xuggle.xuggler.{IPixelFormat, ICodec}
import edu.pk.hach.protocol.meta.Server

object Bootstrap extends SwingApplication {
    implicit val actorSystem = ActorSystem("hach-client")

    val Configuration = {
        val cfg = ConfigFactory.load().getConfig("hach.client")

        Config(
            cfg.getConfigList("servers").map(c => Server(c.getString("host"), c.getInt("port"))).toList,
            cfg.getLong("timeout"),
            {
                val vc = cfg.getConfig("video")
                val size = vc.getString("size").split("x").map(_.toInt)
                VideoConfig(
                    vc.getString("driver"),
                    vc.getString("device"),
                    new Dimension(size(0), size(1)),
                    ICodec.ID.valueOf("CODEC_ID_" + vc.getString("codec")),
                    IPixelFormat.Type.valueOf(vc.getString("pixel-format")),
                    vc.getInt("fps")
                )
            },
            {
                val ac = cfg.getConfig("audio")
                AudioConfig(
                    ICodec.ID.valueOf("CODEC_ID_" + ac.getString("codec")),
                    ac.getInt("sample-rate"),
                    ac.getInt("channels"),
                    ac.getInt("frame-size")
                )
            }
        )
    }

    override def startup(args: Array[String]) {
        new MainWindow(Configuration).visible = true
    }
}
