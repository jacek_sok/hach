package edu.pk.hach.client

import akka.actor.{Actor, ActorSystem}
import akka.actor.ActorDSL._
import akka.pattern.ask
import akka.io.IO
import spray.can.Http
import spray.http.HttpHeaders.Host
import edu.pk.hach.protocol.model.{PingResponseFactory, PingRequest}
import edu.pk.hach.client.Stream.server2host
import scala.concurrent.duration.DurationInt
import scala.concurrent.duration.DurationDouble
import scala.util.Success
import org.slf4j.LoggerFactory
import akka.util.Timeout
import org.joda.time.DateTime
import edu.pk.hach.protocol.meta.Server
import java.util.concurrent.locks.ReentrantLock

class Connector(servers: List[Server])(implicit actorSystem: ActorSystem) {
    object Status extends Enumeration {
        type StatusType = Value
        val Unknown, Ok, Down = Value
    }

    import Connector._
    import Status._
    import actorSystem.dispatcher

    private val status = scala.collection.concurrent.TrieMap[Server, Status.StatusType](servers.map(_ -> Unknown): _*)
    private implicit val timeout = new Timeout(5 seconds)

    class CyclicIterator[T](data: List[T]) {
        private val len = data.length
        private var i = 0
        private val lock = new ReentrantLock()
        def next(condition: T => Boolean): T = {
            lock.lock()
            try {
                for {
                    j <- 0 to len
                    t = data((i + j) % len)
                    if condition(t)
                } try {
                    return t
                } finally {
                    i = (i + j + 1) % len
                }
                null.asInstanceOf[T]
            } finally lock.unlock()
        }
    }

    class RetryHandler extends Actor {
        def receive = {
            case CheckServer(server, attempt) =>
                Log.info("Trying to re-establish connection to: {}", server)
                val rq = PingRequest.toHttpRequest.withHeaders(List[Host](server))
                (IO(Http) ? rq).onComplete {
                    case Success(PingResponseFactory(reply)) =>
//                        Log.info("Connection re-established to {} (server reply: {})", Array[AnyRef](server, reply):_*)
                        status(server) = Ok
                    case rs =>
//                        Log.warn("Server still unavailable: {} (attempt: {}), response: {}", Array[AnyRef](server, attempt.asInstanceOf[Integer], rs):_*)
                        val delay = if (attempt < 5) (math.pow(3, attempt) * 1000).milliseconds else 10.minutes

//                        Log.info("Scheduling retry for server: {} to {}", Array[AnyRef](server, DateTime.now().plusMillis(delay.toMillis.toInt)):_*)
                        actorSystem.scheduler.scheduleOnce(delay, self, CheckServer(server, attempt + 1))
                }

            case e => Log.warn("Unhandled event: {}", e)
        }
    }

    private val retryHandler = actor(new RetryHandler)
    private val iterator = new CyclicIterator(servers)

    def markDown(server: Server) = {
        status(server) = Down
        actorSystem.scheduler.scheduleOnce(5 seconds, retryHandler, CheckServer(server, 2))
    }

    def getServer = iterator.next(status(_) != Down) match {
        case null => throw new IllegalStateException("No connector available")
        case server =>
            Log.debug("Selected server: {}", server)
            server
    }

    def isDown(server: Server) = status.get(server) match {
        case Some(Down) => true
        case _ => false
    }
}

object Connector {
    val Log = LoggerFactory.getLogger(classOf[Connector])

    case class CheckServer(server: Server, attempt: Int)

}