package edu.pk.hach.client

import org.slf4j.LoggerFactory
import edu.pk.hach.client.event.ProtocolListener
import edu.pk.hach.protocol.SessionStatus.SessionStatusType
import edu.pk.hach.protocol.model.PeerInfo
import akka.actor.ActorDSL._
import edu.pk.hach.client.Stream._
import scala.util.Random
import scala.concurrent.duration.DurationInt
import akka.actor.{ActorRef, ActorSystem}
import java.util.concurrent.{TimeUnit, CountDownLatch}
import java.io.FileOutputStream
import javax.management.remote.{JMXConnectorFactory, JMXServiceURL}
import java.lang.management._
import com.sun.management
import org.apache.zookeeper.{WatchedEvent, Watcher, ZooKeeper}
import java.nio.ByteBuffer
import edu.pk.hach.client.Stream.Listen
import edu.pk.hach.client.Stream.PeerNotAvailable
import edu.pk.hach.client.Stream.VideoSlice
import edu.pk.hach.client.Stream.AudioSlice
import java.text.SimpleDateFormat
import java.util.Date
import java.lang.System.{currentTimeMillis => now, err => stderr, exit}
import java.lang.Thread.sleep
import scala.collection.mutable.ListBuffer

object Benchmark extends App {
    implicit val actorSystem = ActorSystem("HachBenchmark")

    import actorSystem.dispatcher

    val Log = LoggerFactory.getLogger(Benchmark.getClass)
    val Configuration = Bootstrap.Configuration
    if (args.length < 3) {
        stderr.println("You need to provide: number of sessions, number of peers in each session and duration of test (in seconds)")
        exit(1)
    }
    val sessions = args(0).toInt
    val peers = args(1).toInt
    val duration = args(2).toInt
    val startNodesMonitors = if (args.length > 3) args(3).toBoolean else false
    val fastStart = if (args.length > 4) args(4).toBoolean else false

    Log.info("Executing benchmark:")
    Log.info("\tNumber of sessions: {}", sessions)
    Log.info("\tNumber of peers in single session: {}", peers)
    Log.info("\tDuration of test: {}s", duration)

    Log.info("Creating threads")
    val latch = new CountDownLatch(sessions)
    val latencyMonitor = actor(new LatencyMonitor)
    val workers = for (i <- 1 to sessions) yield new Worker(peers, latch, latencyMonitor)
    val threads = workers.map(new Thread(_))
    Log.info("Starting...")
    val zkReader = new ZkReader("db_03:2181")
    val latencyMonitorJob = actorSystem.scheduler.schedule(0 seconds, 1 second, latencyMonitor, "aggregate")

    monitor {
        threads.foreach(_.start)
        if (latch.await(60, TimeUnit.SECONDS)) Log.info("Waiting {}s...", duration)
        else Log.warn("Latch timeout")

        sleep(duration * 1000)
        Log.info("Duration time elapsed")

        Log.info("Stopping threads...")
        workers.foreach(_.stop())

        sleep(sessions * peers * 50)
        Log.info("Done")

        sleep(5000)

        latencyMonitor ! "stop"
        latencyMonitorJob.cancel()
    }
    System.exit(0)

    def monitor(a: => Unit) {
        if (startNodesMonitors) {
            val nodesMonitors = Configuration.servers.map(s => actor(new NodeMonitor(s.host, 8010, zkReader)))
            nodesMonitors.foreach(m => actorSystem.scheduler.schedule(0 seconds, 1 second, m, "stats"))
            a
            nodesMonitors.foreach(_ ! "stop")
        } else a
    }

    class Worker(peersNumber: Int, latch: CountDownLatch, latencyMonitor: ActorRef) extends Runnable {
        private var $stop = false
        private var clients: Seq[Client] = _

        def run() {
            var listOfPeers: Set[PeerInfo] = Set()
            val guests = for (i <- 1 to peersNumber - 1) yield new Client(Configuration, new ProtocolListenerImpl(i))
            val listener = new ProtocolListenerImpl(0) {
                override def onSessionCreated(session: String, exchange: String): Unit = {
                    guests.foreach(c => {
                        workers.synchronized {
                            c.start(c.uuid.toString, exchange)
                            condSleep(100 + new Random(now).nextInt(500))
                        }
                    })
                }

                override def onSessionRead(session: String, status: SessionStatusType, peers: Set[PeerInfo], server: String): Unit = {
                    listOfPeers = peers
                }
            }
            val originator = new Client(Configuration, listener)
            originator.start(originator.uuid.toString)

            while (listOfPeers.size < peersNumber) sleep(100)

            clients = originator :: guests.toList
            clients.foreach(c => {
                val myName = c.uuid.toString
                guests.synchronized {
                    condSleep(100 + new Random(now).nextInt(600))
                    c.send(actor(myName + "$Sender$Actor")(new Act {
                        become {
                            case Listen(actor) =>
                                val rand = new Random(System.currentTimeMillis())
                                while (!$stop) {
                                    val data =
                                        if (rand.nextDouble() > 0.7) VideoSlice({
                                            val arr = randBits(rand, 6000)
                                            var i = 0
                                            ByteBuffer.allocate(8).putLong(now).array().foreach(b => {
                                                arr(i) = b
                                                i += 1
                                            })
                                            arr
                                        })
                                        else AudioSlice(randBits(rand, 2000))
                                    actor ! data
                                    sleep(40)
                                }
                                actor ! Empty
                                c.stop()
                        }
                    }))
                }
                listOfPeers.foreach(p => {
                    if (p.name != myName) {
                        c.receive(p.id, getReceiver(myName, p.id, c))
                    }
                })
            })
            latch.countDown()
        }

        def condSleep(t: Long) = if (!fastStart) sleep(t)

        def randBits(rand: Random, len: Long) = {
            val arrayLength = (len * 0.8) + rand.nextInt((len * 0.2).toInt)
            val array = new Array[Byte](arrayLength.toInt)
            rand.nextBytes(array)
            array
        }

        def stop() {
            $stop = true
        }

        def getReceiver(name: String, peer: String, client: Client) = actor(new Act {
            become {
                case data: StreamSlice =>
                    if (data.isInstanceOf[VideoSlice]) {
                        val sentTime = ByteBuffer.wrap(data.stream.take(8)).getLong
                        val latency = now - sentTime
                        latencyMonitor ! latency
                    }
                    if ($stop) {
                        sleep(100 + new Random().nextInt(200))
                        context.stop(self)
                    }
                case Empty =>
                    context.stop(self)
                case PeerNotAvailable(_) =>
                    sleep(300)
                    client.receive(peer, self)
            }
        })

        class ProtocolListenerImpl(no: Int) extends ProtocolListener {
            override def onConnectionError(reason: String): Unit = {}

            override def onStreamReceiveError(session: String, peer: String, error: String): Unit = {}

            override def onCreatingSession(peerName: String): Unit = {}

            override def onAttachingToSession(session: String, peerName: String): Unit = {}

            override def onStreamClosed(session: String, peer: String): Unit = {
                if (!$stop) {
                    val client = clients(no)
                    val name = client.uuid.toString
                    sleep(500)
                    client.receive(peer, getReceiver(name, peer, client))
                }
            }

            override def onReadingSession(session: String): Unit = {}

            override def onAttachedToSession(session: String): Unit = {}

            override def onSessionCreated(session: String, exchange: String): Unit = {}

            override def onSessionNotFound(): Unit = {}

            override def onSessionRead(session: String, status: SessionStatusType, peers: Set[PeerInfo], server: String): Unit = {}

            override def onStreamSendError(session: String, error: String): Unit = {}
        }

    }

    class NodeMonitor(node: String, port: Int, zk: ZkReader) extends Act {

        import java.lang.management.ManagementFactory._

        val (connection, mbean) = {
            val url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://%s:%d/jmxrmi".format(node, port))
            val jmxc = JMXConnectorFactory.connect(url, null)
            val mbsc = jmxc.getMBeanServerConnection

            (jmxc, mbsc)
        }
        val systemMBean = getPlatformMXBean(mbean, classOf[management.OperatingSystemMXBean])
        val memoryMBean = getPlatformMXBean(mbean, classOf[MemoryMXBean])
        val threadMBean = getPlatformMXBean(mbean, classOf[ThreadMXBean])
        val out = new FileOutputStream("stats_" + node + ".txt")
        val DateFormat = new SimpleDateFormat("mm:hh:ss.SSS")

        become {
            case "stop" =>
                tryWithDefault((), connection.close())
                tryWithDefault((), out.close())

            case "stats" =>
                val time = DateFormat.format(new Date)
                val cpu = tryWithDefault(0d, systemMBean.getSystemCpuLoad * 100)
                val memory = tryWithDefault(0d, (memoryMBean.getHeapMemoryUsage.getUsed / memoryMBean.getHeapMemoryUsage.getMax.asInstanceOf[Double]) * 100)
                val threads = tryWithDefault(0, threadMBean.getThreadCount)
                val sessions = tryWithDefault(0, zk.stats(node + ":8080"))
                out.write("\"%s\"\t%.2f\t%.2f\t%d\t%d\n".format(time, cpu, memory, threads, sessions).getBytes)
        }

        def tryWithDefault[T](default: T, a: => T) = {
            try a catch {
                case any: Exception => default
            }
        }
    }

    class ZkReader(host: String) extends Watcher {
        val zk = new ZooKeeper(host, 5000, this)

        def stop() = zk.close()

        def stats(server: String) = ByteBuffer.wrap(zk.getData("/stat/" + server, false, null)).getInt

        override def process(event: WatchedEvent): Unit = {}
    }

    class LatencyMonitor extends Act {
        val out = new FileOutputStream("latency.txt")
        val buffer = ListBuffer[Long]()
        become {
            case "stop" =>
                out.close()
            case l: Long => buffer += l
            case "aggregate" =>
                val avg =
                    if (buffer.isEmpty) 0
                    else buffer.foldLeft(0L)((sum, l) => sum + l) / buffer.size.asInstanceOf[Double]
                out.write((avg + "\n").getBytes)
                buffer.clear()
                out.flush()
        }
    }
}
