package edu.pk.hach.client.ui

import akka.actor.{ActorSystem, Props, Actor}
import com.xuggle.xuggler._
import com.xuggle.xuggler.IStreamCoder.Direction
import com.xuggle.xuggler.video.ConverterFactory
import scala.swing.{Graphics2D, Panel, Frame}
import com.xuggle.ferry.IBuffer
import java.awt.image.BufferedImage
import javax.sound.sampled._
import scala.collection.mutable.ArrayBuffer
import edu.pk.hach.client.Config
import edu.pk.hach.client.Stream.{CleanUp, VideoSlice, AudioSlice}
import scala.swing.event.WindowClosing

class Player(config: Config, frameTitle: String, mute: Boolean = false)(implicit actorSystem: ActorSystem) extends Frame {
    private var image: BufferedImage = _
    val receiver = actorSystem.actorOf(Props.create(classOf[StreamReceiver], this))

    contents = new Panel {
        peer.setDoubleBuffered(true)

        override protected def paintComponent(g: Graphics2D) {
            if (image != null) g.drawImage(image, 0, 0, null)
        }
    }

    title = frameTitle
    size = config.video.size
    visible = true
    resizable = false

    reactions += {
        case WindowClosing(_) =>
            receiver ! "Stop"
    }

    abstract class StreamDecoder {
        val buffer = ArrayBuffer[Byte]()

        def clear() = buffer.clear()

        def decode(packet: IPacket)

        def feed(stream: Array[Byte]) {
            buffer ++= stream
            if (!buffer.isEmpty) {
                val packet = IPacket.make(IBuffer.make(null, buffer.size))
                try {
                    packet.put(buffer.toArray, 0, 0, buffer.size)
                    if (packet.isComplete) try decode(packet) finally buffer.clear()
                } finally packet.delete()
            }
        }
    }

    class StreamReceiver extends Actor {
        import scala.language.reflectiveCalls

        val cfg = config.video
        val videoStream = new StreamDecoder{
            val videoCoder = IStreamCoder.make(Direction.DECODING, cfg.codec)
            videoCoder.open(null, null)

            override def decode(packet: IPacket) {
                val picture = IVideoPicture.make(cfg.pixelFormat, cfg.size.width, cfg.size.height)
                try {
                    // decode the packet into the video picture
                    var position = 0
                    val packetSize = packet.getSize
                    while (position < packetSize) {
                        position += videoCoder.decodeVideo(picture, packet, position)
                        // if this is a complete picture, dispatch the picture
                        if (picture.isComplete) {
                            val converter = ConverterFactory.createConverter(ConverterFactory.XUGGLER_BGR_24, picture)
                            image = converter.toImage(picture)
                            repaint()
                            converter.delete()
                        } else return
                        // clean the picture and reuse it
                        picture.getByteBuffer.clear()
                    }
                } finally picture.delete()
            }
        }

        val audioStream = new StreamDecoder {
            val cfg = config.audio
            val audioCoder = IStreamCoder.make(Direction.DECODING, cfg.codec)
            audioCoder.setChannels(cfg.channels)
            audioCoder.open(null, null)

            val audioFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, cfg.sampleRate, 16, cfg.channels, cfg.frameSize, cfg.sampleRate, false)
            val dataLineInfo = new DataLine.Info(classOf[SourceDataLine], audioFormat)
            var line = AudioSystem.getLine(dataLineInfo).asInstanceOf[SourceDataLine]

            line.open(audioFormat)
            line.start()

            override def decode(packet: IPacket) {
                var offset = 0
                while (offset < packet.getSize) {
                    val samples = IAudioSamples.make(512, cfg.channels)
                    offset = audioCoder.decodeAudio(samples, packet, offset)
                    if (samples.isComplete) {
                        val size = samples.getSize
                        val buffer = new Array[Byte](size)
                        samples.get(0, buffer, 0, size)
                        line.write(buffer, 0, size)
                    }
                    samples.delete()
                }
            }
        }

        override def postStop() {
            audioStream.audioCoder.close()
            audioStream.line.close()
            videoStream.videoCoder.close()
        }

        override def receive = {
            case VideoSlice(stream) => videoStream.feed(stream)
            case AudioSlice(stream) => if (!mute) audioStream.feed(stream)
            case CleanUp =>
                videoStream.clear()
                audioStream.clear()
            case "Stop" => context.stop(self)
        }
    }
}
