package edu.pk.hach.client.ui

import scala.swing._
import edu.pk.hach.protocol.model.PeerInfo
import scala.swing.event.{MouseClicked, WindowClosing, ValueChanged, ButtonClicked}
import java.awt.{Font, Color, Dimension}
import scala.swing.ListView.Renderer
import akka.actor.{Props, ActorRef, ActorSystem}
import scala.swing.Dialog.Message
import edu.pk.hach.client.{Client, Config}
import edu.pk.hach.client.Client._
import edu.pk.hach.client.event.ProtocolListener
import edu.pk.hach.protocol.SessionStatus.SessionStatusType
import scala.swing.Font
import edu.pk.hach.client.Stream.CleanUp

class MainWindow(config: Config)(implicit actorSystem: ActorSystem) extends MainFrame {
    val client = new Client(config, new EventListener)
    val frame = this
    val Width = 500
    val HeightIdle = 180
    val HeightActive = 260
    var capturer: ActorRef = _
    var players = Map[String, Player]()

    class EventListener extends ProtocolListener {
        def onConnectionError(reason: String) {
            reset()
            Dialog.showMessage(nameField, "Failed to connect to the server\n" + reason, "Connection Error", Message.Error)
        }

        def onSessionNotFound() {
            reset()
            val msg = if (client.state == Ready) "Session closed by originator" else "Cannot find given session"
            Dialog.showMessage(nameField, msg, "Session not available", Message.Error)
        }

        def onCreatingSession(peerName: String) {
            statusLabel.text = "creating session..."
        }

        def onSessionCreated(session: String, exchange: String) {
            sessionField.text = exchange
            sessionField.foreground = Color.BLUE
        }

        def onAttachingToSession(session: String, peerName: String) {
            statusLabel.text = "session created, attaching..."
        }

        def onAttachedToSession(session: String) {
            statusLabel.text = "peer attached"
        }

        def onReadingSession(session: String) {
            statusLabel.text = "reading session"
        }

        def onSessionRead(session: String, status: SessionStatusType, peers: Set[PeerInfo], server: String) {
            statusLabel.text = "connected"
            peersList.listData = peers.toSeq

            peersListPane.visible = true
            frame.size = new Dimension(Width, HeightActive)
        }

        def onStreamClosed(session: String, peer: String) {
            Log.info("Stream closed for peer: {}", peer)
            players.get(peer) match {
                case Some(p) => p.dispose()
                case _ =>
            }
        }

        def onStreamReceiveError(session: String, peer: String, error: String) {
        }

        def onStreamSendError(session: String, error: String) {
            peersList.listData.find(_.name == nameField.text) match {
                case Some(p) => players(p.id).receiver ! CleanUp
                case _ =>
            }
        }
    }

    object nameLabel extends Label {
        text = "Your name:"
    }

    object nameField extends TextField {
        columns = 30
    }

    object sessionLabel extends Label {
        text = "Session ID: "
    }

    object sessionField extends TextField {
        columns = 100
    }

    object statusLabel extends Label {
        font = new Font(font.getName, Font.ITALIC, font.getSize)
        text = "disconnected"
        horizontalAlignment = Alignment.Left
    }

    object engageButton extends Button {
        text = "CREATE"
        listenTo(sessionField)
        reactions += {
            case ValueChanged(field: TextField) if client.state == Idle =>
                text = if (field.text.trim.isEmpty) "CREATE" else "JOIN"
            case ButtonClicked(_) => text match {
                case "CLOSE" =>
                    client.stop()
                    reset()
                case _ if !nameField.text.trim.isEmpty =>
                    text = "CLOSE"
                    nameField.editable = false
                    sessionField.editable = false
                    client.start(nameField.text, if (sessionField.text.isEmpty) null else sessionField.text)
                case _ =>
                    nameField.requestFocus()
                    Dialog.showMessage(nameField, "Please provide your name", "Invalid input", Message.Warning);
            }
        }
    }

    object peersList extends ListView[PeerInfo] {
        renderer = Renderer(_.name)
    }

    object peersListPane extends ScrollPane {
        contents = peersList
        visible = false
    }

    lazy val ui = new GridBagPanel {

        import GridBagPanel.Anchor._
        import GridBagPanel.Fill._

        val c = new Constraints
        c.fill = Horizontal
        c.weighty = 0.0

        c.weightx = 0.1
        c.anchor = PageStart
        layout(nameLabel) = c

        c.weightx = 0.9
        c.gridx = 1
        layout(nameField) = c

        c.gridx = 0
        c.gridy = 1
        c.weightx = 0.1
        layout(sessionLabel) = c

        c.weightx = 0.9
        c.gridx = 1
        layout(sessionField) = c

        c.gridy = 2
        layout(new Label(" ")) = c

        c.gridx = 0
        c.gridy = 3
        c.weightx = 1.0
        c.gridwidth = 2
        c.ipady = 5
        layout(engageButton) = c

        c.ipady = 0
        c.gridy = 4
        layout(new Label(" ")) = c

        c.gridy = 5
        c.weighty = 1.0
        c.fill = Both
        layout(peersListPane) = c

        c.anchor = LastLineStart
        c.gridy = 6
        c.weighty = 0.0
        c.ipady = 5
        c.ipadx = 0
        c.gridwidth = 2
        c.fill = Horizontal
        layout(statusLabel) = c

        border = Swing.EmptyBorder(15, 15, 5, 15)
    }
    contents = ui

    title = "HACH Client"
    size = new Dimension(Width, HeightIdle)
    resizable = false
    listenTo(peersList.mouse.clicks)

    reactions += {
        case WindowClosing(src) if client.state != Idle =>
            if (src.self == self) {
                client.stop()
                Thread.sleep(500)
            } else {
                val peerName = src.asInstanceOf[Player].title
                peersList.listData.find(_.name == peerName) match {
                    case Some(p) =>
                        players -= p.id
                        if (peerName == nameField.text) stopCapturer()
                    case None => Log.warn("No such peer on the list: {}", peerName)
                }
            }

        case m: MouseClicked if m.clicks == 2 =>
            val peer = peersList.listData(peersList.selection.anchorIndex)
            val player = players.get(peer.id) match {
                case None =>
                    val originator = peer.name == nameField.text
                    val p = new Player(config, peer.name, originator)
                    if (originator) {
                        if (capturer == null) {
                            capturer = actorSystem.actorOf(Props(classOf[Capturer], config, p.receiver))
                            client.send(capturer)
                        }
                    } else
                        client.receive(peer.id, p.receiver)
                    players += peer.id -> p
                    listenTo(p)
                    p
                case Some(p) => p
            }
            player.visible = true
    }

    def reset() {
        stopCapturer()

        players.values.foreach(_.dispose())
        players = Map[String, Player]()

        engageButton.text = "CREATE"
        nameField.editable = true
        sessionField.editable = true
        sessionField.text = ""
        sessionField.foreground = Color.BLACK
        peersListPane.visible = false
        frame.size = new Dimension(Width, HeightIdle)

        statusLabel.text = "disconnected"
    }

    def stopCapturer() {
        if (capturer != null) {
            capturer ! "Stop"
            capturer = null
        }
    }
}
