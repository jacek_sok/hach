package edu.pk.hach.client

import akka.testkit.{TestKit, ImplicitSender}
import akka.actor.ActorSystem
import org.scalatest.{BeforeAndAfter, OptionValues, Matchers, FlatSpecLike}
import scala.concurrent.duration.Duration

abstract class UnitSpec extends TestKit(ActorSystem("hach-test")) with ImplicitSender with FlatSpecLike with Matchers with OptionValues with BeforeAndAfter {
    implicit def durationToLong(duration: Duration) = {
        duration.toMillis
    }
}