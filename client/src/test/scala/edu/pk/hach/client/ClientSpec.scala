package edu.pk.hach.client

import scala.concurrent.duration.DurationInt
import org.mockito.Mockito._
import org.mockito.Matchers._
import java.lang.Thread.sleep
import edu.pk.hach.protocol.SessionStatus.SessionStatusType
import org.scalatest.BeforeAndAfterAll
import edu.pk.hach.protocol.meta.Server

//import edu.pk.hach.server.fixture.EmbedableHachServer
import org.mockito.ArgumentCaptor
import edu.pk.hach.client.event.ProtocolListener
import edu.pk.hach.protocol.model.PeerInfo
import akka.testkit.TestProbe
import edu.pk.hach.client.Stream.{VideoSlice, Empty, Listen}
import org.scalatest.concurrent.ScalaFutures

class ClientSpec extends UnitSpec with BeforeAndAfterAll with ScalaFutures {
    val config = Config(List(Server("localhost", 8196)), 1 second, null, null)
    val peerName = "ludomir"

//    override protected def beforeAll() {
//        EmbedableHachServer.start(config.servers(0).host, config.servers(0).port)
//    }
//
//    override protected def afterAll() {
//        EmbedableHachServer.stop()
//    }

    ignore should "create new session, initialize peer and wait for other peers" in {
        val listener = mock(classOf[ProtocolListener])
        val streamer = new Client(config, listener)

        streamer.start(peerName)
        sleep(2 seconds)

        verify(listener).onCreatingSession(peerName)
        verify(listener).onSessionCreated(anyString, anyString)
        verify(listener).onAttachingToSession(anyString, anyString)
        verify(listener).onAttachedToSession(anyString)
        verify(listener).onReadingSession(anyString)
        verify(listener).onSessionRead(anyString, any(classOf[SessionStatusType]), any(classOf[Set[PeerInfo]]), anyString)
    }
    ignore should "create pair communication" in {
        val listener1 = mock(classOf[ProtocolListener])
        val listener2 = mock(classOf[ProtocolListener])

        val streamerA = new Client(config, listener1)
        val streamerB = new Client(config, listener2)

        streamerA.start("A")
        sleep(2 seconds)

        val sessionCaptor = ArgumentCaptor.forClass(classOf[String])
        verify(listener1).onSessionCreated(anyString, sessionCaptor.capture())
        val exchange = sessionCaptor.getValue

        streamerB.start("B", exchange)
        sleep(1 second)

        val peersCaptor = ArgumentCaptor.forClass(classOf[Set[PeerInfo]])
        verify(listener2).onSessionRead(anyString, any(classOf[SessionStatusType]), peersCaptor.capture(), anyString)
        val peers = peersCaptor.getValue
        peers should not be null
        peers.size should equal(2)
        peers.map(_.name) should contain allOf("A", "B")

        streamerA.stop()
        streamerB.stop()
        sleep(1 second)
    }
    ignore should "send stream from A to B" in {
        val listener1 = mock(classOf[ProtocolListener])
        val listener2 = mock(classOf[ProtocolListener])

        val rcv, snd = TestProbe()

        val streamerA = new Client(config, listener1)
        val streamerB = new Client(config, listener2)

        streamerA.start("A")
        sleep(1 second)

        val sessionCaptor = ArgumentCaptor.forClass(classOf[String])
        val peerCaptor = ArgumentCaptor.forClass(classOf[Set[PeerInfo]])
        verify(listener1).onSessionCreated(anyString, sessionCaptor.capture())
        verify(listener1, atLeastOnce()).onSessionRead(anyString, any(classOf[SessionStatusType]), peerCaptor.capture(), anyString)

        streamerB.start("B", sessionCaptor.getValue)
        sleep(1 second)

        new Producer(snd, "SENDER_A")
        val msg = new Receiver(rcv)

        streamerA.send(snd.ref)
        streamerB.receive(peerCaptor.getValue.iterator.next().id, rcv.ref)

        sleep(1 second)

        msg.data should equal("SENDER_A")

        streamerA.stop()
        streamerB.stop()

        sleep(1 second)
    }

    class Producer(probe: TestProbe, data: String) {
        new Thread(new Runnable {
            override def run(): Unit = {
                val receiver = probe.expectMsgClass(classOf[Listen]).actor
                Thread.sleep(500)
                data.foreach(s => receiver ! VideoSlice(Character.toString(s).getBytes))
                receiver ! Empty
            }
        }).start()
    }

    class Receiver(probe: TestProbe) {
        var data: String = _

        new Thread(new Runnable {
            override def run(): Unit = {
                data = probe.receiveWhile() {
                    case VideoSlice(s) => new String(s)
                }.mkString("")
                println(data)
            }
        }).start()
    }
}
