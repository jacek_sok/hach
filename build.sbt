name := "HACH"

description := "Highly-available Audio/Video Communication via HTTP"

lazy val root = project
    .in(file("."))
    .settings(defaultSettings: _*)
    .aggregate(protocol, client, server)

lazy val protocol = project
    .settings(defaultSettings: _*)

lazy val client = project
    .settings(defaultSettings: _*)
    .dependsOn(protocol)

lazy val server = project
    .settings(defaultSettings: _*)
    .dependsOn(protocol)