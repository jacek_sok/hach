import sbt._
import sbt.Keys._

object HachBuild extends Build {
    val Slf4jVersion = "1.6.6"
    val SprayVersion = "1.3.1"
    val SpringVersion = "3.2.5.RELEASE"
    val AkkaVersion = "2.3.0"

    lazy val defaultSettings =
        Defaults.defaultSettings ++
            net.virtualvoid.sbt.graph.Plugin.graphSettings ++
            Seq(
                version := "1.0",
                scalaVersion := "2.10.3",
                scalacOptions := Seq(
                    "-feature",
                    "-unchecked",
                    "-deprecation",
                    "-encoding", "utf8",
                    "-language:implicitConversions",
                    "-language:postfixOps"
                ),
                libraryDependencies ++= Seq(
                    "org.slf4j" % "jcl-over-slf4j" % Slf4jVersion,
                    "com.typesafe.akka" %% "akka-actor" % AkkaVersion,
                    "ch.qos.logback" % "logback-classic" % "1.0.13" exclude("org.slf4j", "slf4j-api"),
                    "org.scalatest" %% "scalatest" % "2.0" % "test",
                    "org.mockito" % "mockito-core" % "1.9.5" % "test"
                ),
                resolvers ++= Seq("Spray Repo" at "http://repo.spray.io")
            )
}
