name := "HACH :: Protocol"

version := "1.0"

libraryDependencies += "io.spray" % "spray-http" % SprayVersion

libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value

libraryDependencies += "com.google.code.gson" % "gson" % "2.2.4"