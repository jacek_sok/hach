package edu.pk.hach.protocol.model

import spray.http._
import spray.http.HttpMethods.POST
import spray.http.Uri._
import edu.pk.hach.protocol.{RequestFactory, SuccessResponse, Request}
import edu.pk.hach.protocol.EntityMarshaller._
import spray.http.StatusCodes._
import spray.http.HttpRequest
import spray.http.HttpResponse
import scala.Some

case class InitPeerRequest(session: String, name: String, ip: String = null) extends Request {

    import InitPeerRequestFactory._

    override def toHttpRequest: HttpRequest =
        HttpRequest(
            method = Method,
            uri = / withPath Path(Url) / session,
            entity = HttpEntity(marshall(NameParam -> name))
        )
}

object InitPeerRequestFactory extends RequestFactory {
    val Url = "/peer"
    val Method = POST
    private[model] val NameParam = "name"

    def unapply(request: HttpRequest): Option[(Option[String], Option[String], String)] = request match {
        case HttpRequest(Method, uri, headers, entity, _) =>
            def result(sess: Option[String]) = Some(sess, unmarshall(entity) *> "name", getRemoteAddress(headers))

            uri match {
                case PathUri(s, _*) =>
                    result(Some(s))
                case PathUri() =>
                    result(None)
                case _ => None
            }
        case _ => None
    }

    private def getRemoteAddress(headers: List[HttpHeader]) = headers.find({
        case x: HttpHeaders.`Remote-Address` => true
        case _ => false
    }) match {
        case None => "localhost"
        case Some(addr) => addr.value
    }
}

case class InitPeerResponse(session: String, id: String) extends SuccessResponse {
    override protected val content: Map[String, String] = Map("session" -> session, "id" -> id)
}

object InitPeerResponseFactory {
    def unapply(response: HttpResponse): Option[String] = response match {
        case HttpResponse(OK, entity, _, _) =>
            val data = unmarshall(entity)
            data *> "session"
        case _ => None
    }
}