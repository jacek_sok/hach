package edu.pk.hach.protocol.model

import edu.pk.hach.protocol._
import edu.pk.hach.protocol.EntityMarshaller._
import spray.http.HttpMethods.GET
import spray.http.{HttpResponse, HttpEntity, HttpRequest}
import spray.http.StatusCodes.OK
import spray.http.Uri.Path
import spray.http.Uri./
import java.lang.String._
import scala.Some

case class InitRequest(multi: Boolean = false) extends Request {

    import InitRequestFactory._

    override def toHttpRequest: HttpRequest = HttpRequest(
        Method,
        / withPath Path(Url),
        entity = marshall(Map(MultiParam -> valueOf(multi)))
    )
}

object InitRequestFactory extends RequestFactory {
    override val Method = GET
    override val Url = "/init"
    private[model] val MultiParam = "multi"

    def unapply(request: HttpRequest): Option[Boolean] = request match {
        case HttpRequest(Method, Path(Url), _, entity: HttpEntity, _) =>
            val data = unmarshall(entity)
            Some(if (data.contains(MultiParam)) data(MultiParam).asInstanceOf[String].toBoolean else false)
        case _ => None
    }
}

case class InitResponse(session: String, exchange: String) extends SuccessResponse {
    override protected val content: Map[String, String] = Map("session" -> session, "exchange" -> exchange)
}

object InitResponseFactory {
    def unapply(response: HttpResponse): Option[(String, String)] = response match {
        case HttpResponse(OK, entity, _, _) =>
            val data = unmarshall(entity)
            Some((data > "session", data > "exchange"))
        case _ => None
    }
}