package edu.pk.hach.protocol.model

import edu.pk.hach.protocol.{SuccessResponse, RequestFactory, Request}
import spray.http.HttpMethods.POST
import spray.http.HttpHeaders.Connection
import spray.http.Uri._
import spray.http.HttpRequest

case class CloseRequest(session: String) extends Request {
    import CloseRequestFactory._
    override def toHttpRequest: HttpRequest = HttpRequest(Method, / withPath Path(Url) / session, List(Connection("close")))
}

object CloseRequestFactory extends RequestFactory {
    override val Url = "/close"
    override val Method = POST

    def unapply(request: HttpRequest): Option[Option[String]] = request match {
        case HttpRequest(Method, uri, _, _, _) => uri match {
            case PathUri(session) => Some(Some(session))
            case PathUri() => Some(None)
            case _ => None
        }
        case _ => None
    }
}

case object CloseResponse extends SuccessResponse