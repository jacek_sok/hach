package edu.pk.hach.protocol

import akka.actor.{Actor, ActorRef}
import spray.http.HttpHeaders.Host
import akka.util.Timeout

trait HttpConverters extends Actor {

    implicit class EnrichedResponseSender(sender: ActorRef) {
        def <~(response: Response) =
            sender ! response.toHttpResponse
    }

    implicit class AsyncRequestSender(sender: ActorRef)(implicit host: Host = null) {
        def ~>(request: Request) =
            sender ! request.toHttpRequest.withHeaders(if (host != null) List(host) else List())
    }

    implicit class SyncRequestSender(sender: ActorRef)(implicit host: Host = null, timeout: Timeout) {
        import akka.pattern.ask
        def <~>(request: Request) =
            sender ? request.toHttpRequest.withHeaders(if (host != null) List(host) else List())
    }
}
