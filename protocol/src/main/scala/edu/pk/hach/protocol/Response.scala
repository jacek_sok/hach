package edu.pk.hach.protocol

import spray.http._
import EntityMarshaller._
import spray.http.HttpResponse

trait Response {
    def toHttpResponse: HttpResponse
}

trait BasicResponse extends Response {
    val status: StatusCode
    protected val headers = List[HttpHeader]()
    protected val content = Map[String, Any]()
    protected def prepareBody = HttpEntity(marshall(content))
    override def toHttpResponse: HttpResponse = HttpResponse(status, prepareBody, headers)
}

trait SuccessResponse extends BasicResponse {
    val status: StatusCode = 200
}