package edu.pk.hach.protocol.meta

case class Server(host: String, port: Int) {
    override def toString: String = host + ":" + port
}

object Server {
    def apply(str: String) = {
        val s = str.split(":")
        new Server(s(0), if (s.length > 1) s(1).toInt else 80)
    }
}
