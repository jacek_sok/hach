package edu.pk.hach.protocol.model

import edu.pk.hach.protocol.SessionStatus.SessionStatusType
import spray.http.{HttpEntity, HttpResponse, HttpRequest}
import spray.http.StatusCodes.OK
import spray.http.HttpMethods.GET
import edu.pk.hach.protocol.{SessionStatus, RequestFactory, SuccessResponse, Request}
import edu.pk.hach.protocol.EntityMarshaller._
import spray.http.Uri.{Path, /}

case class StatusRequest(session: String) extends Request {

    import StatusRequestFactory._

    override def toHttpRequest: HttpRequest = HttpRequest(Method, / withPath Path(Url) / session)
}

object StatusRequestFactory extends RequestFactory {
    val Method = GET
    val Url = "/status"

    def unapply(request: HttpRequest): Option[Option[String]] = request match {
        case HttpRequest(Method, uri, _, _, _) => uri match {
            case PathUri(session, _*) => Some(Some(session))
            case x if x.toString().endsWith(Url) => Some(None)
            case _ => None
        }
        case _ => None
    }
}

case class StatusResponse(sessionStatus: SessionStatusType, peers: Set[PeerInfo], serverString: String) extends SuccessResponse {
    override protected val content = Map(
        "status" -> sessionStatus.toString,
        "peers" -> peers,
        "serverString" -> serverString
    )

    override protected def prepareBody: HttpEntity = HttpEntity(
        """{
          |     "timestamp": "%s",
          |     "status": "%s",
          |     "peers": [
          |         %s
          |     ],
          |     "serverString": "%s"
          |}""".stripMargin.format(
                System.currentTimeMillis(),
                sessionStatus.toString,
                peers.map(peer =>
                    """{
                      |     "id": "%s",
                      |     "name": "%s",
                      |     "host": %s
                      |}""".stripMargin.format(
                            peer.id,
                            peer.name,
                            if (peer.host != null) "\"" + peer.host + "\"" else null)).mkString(","),
                serverString)
    )
}

object StatusResponseFactory {
    def unapply(response: HttpResponse): Option[(SessionStatusType, Set[PeerInfo], String)] = response match {
        case HttpResponse(OK, entity, _, _) =>
            val data = unmarshall(entity)
            val peers = (data > "peers").asInstanceOf[List[Map[String, String]]].toSet[Map[String, String]]
            Some((SessionStatus.withName(data > "status"), peers.map(PeerInfo(_)), data > "serverString"))
        case _ => None
    }
}

case class PeerInfo(id: String, name: String, host: String)

object PeerInfo {
    def apply(map: Map[String, String]) = new PeerInfo(map("id"), map("name"), map.getOrElse("host", null))
}