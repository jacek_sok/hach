package edu.pk.hach.protocol

object SessionStatus extends Enumeration {
    type SessionStatusType = Value
    val Opened, Closed, Expired = Value
}

object PeerStatus extends Enumeration {
    type PeerStatusType = Value
    val Ready, Streaming, Closed, Retry = Value
}
