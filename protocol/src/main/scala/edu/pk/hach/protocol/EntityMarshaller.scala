package edu.pk.hach.protocol

import org.slf4j.LoggerFactory
import com.google.gson.Gson
import collection.JavaConversions._
import spray.http.HttpEntity
import java.util
import org.parboiled.common.StringUtils
import com.google.gson.internal.LinkedTreeMap

object EntityMarshaller {
    val Log = LoggerFactory.getLogger(EntityMarshaller.getClass)

    def marshall(params: (String, String)*): String = marshall(Map(params: _*))

    def marshall(params: Map[String, Any]): String = {
        val map = params + ("timestamp" -> System.currentTimeMillis())
        new Gson().toJson(mapAsJavaMap(map))
    }

    def unmarshall(entity: HttpEntity): Map[String, Any] = unmarshall(entity.asString)

    def unmarshall(stream: String): Map[String, Any] =
        if (StringUtils.isEmpty(stream)) Map()
        else refactorMaps(mapAsScalaMap(new Gson().fromJson(stream, classOf[util.HashMap[String, Any]])).toMap)

    implicit class Extractor(map: Map[String, Any]) {
        def >[T](key: String): T = map(key).asInstanceOf[T]
        def *>[T](key: String): Option[T] = map.get(key).asInstanceOf[Option[T]]
    }

    private def refactorMaps(data: Map[String, Any]): Map[String, Any] = data.mapValues(v => refactorValues(v))

    private def refactorValues(data: Any): Any = data match {
        case map: LinkedTreeMap[_, _] => (for ((a, b) <- map) yield a -> refactorValues(b)).toMap
        case array: util.ArrayList[_] => array.map(refactorValues).toList
        case _ => data
    }
}
