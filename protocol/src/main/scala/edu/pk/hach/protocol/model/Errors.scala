package edu.pk.hach.protocol.model

import spray.http.{HttpEntity, StatusCode}
import spray.http.StatusCodes._
import edu.pk.hach.protocol.{BasicResponse, Response}
import spray.http.HttpResponse

case class ErrorResponse(reason: String, request: AnyRef, status: StatusCode = 500, stackTrace: Array[StackTraceElement] = Array()) extends Response {
    protected val content = "Reason: %s\nRequest: %s\nDebug:%s\n".format(reason, request, if (stackTrace.isEmpty) " n/a" else "\n" + stackTrace.mkString("\n\t"))

    override def toHttpResponse: HttpResponse = HttpResponse(status, HttpEntity(content))
}

case object SessionNotFound extends BasicResponse {
    override val status: StatusCode = NotFound
}

object SessionNotFoundFactory {
    def unapply(response: HttpResponse): Option[HttpResponse] = response match {
        case HttpResponse(SessionNotFound.status, _, _, _) => Some(response)
        case _ => None
    }
}

case object InvalidSession extends BasicResponse {
    override val status: StatusCode = BadRequest
}

case object StreamNotReady extends BasicResponse {
    override val status: StatusCode = Accepted
}