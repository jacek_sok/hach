package edu.pk.hach.protocol

import spray.http.HttpRequest

trait Request {
    def toHttpRequest: HttpRequest
}