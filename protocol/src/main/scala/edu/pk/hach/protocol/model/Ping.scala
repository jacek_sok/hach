package edu.pk.hach.protocol.model

import edu.pk.hach.protocol.{Response, RequestFactory, Request}
import spray.http.HttpResponse
import spray.http.HttpMethods.HEAD
import spray.http.StatusCodes.NoContent
import spray.http.HttpHeaders.RawHeader
import spray.http.Uri._
import spray.http.HttpResponse
import spray.http.HttpRequest
import spray.http.HttpResponse
import spray.http.HttpHeaders.RawHeader
import scala.Some

case object PingRequest extends Request {
    import PingRequestFactory._

    override def toHttpRequest: HttpRequest = HttpRequest(Method, / withPath Path(Url))
}

object PingRequestFactory extends RequestFactory {
    val Method = HEAD
    val Url = "/ping"

    def unapply(request: HttpRequest): Option[Long] = request match {
        case HttpRequest(Method, uri, _, _, _) => Some(System.currentTimeMillis())
        case _ => None
    }
}

case class PingResponse(server: String) extends Response {
    override def toHttpResponse: HttpResponse = HttpResponse(NoContent, headers = List(RawHeader("Pong", server)))
}

object PingResponseFactory {
    def unapply(response: HttpResponse): Option[String] = response match {
        case HttpResponse(NoContent, _, headers, _) =>
            headers.find(_.name == "Pong") match {
                case None => Some(null)
                case Some(h) => Some(h.value)
            }
        case _ => None
    }
}