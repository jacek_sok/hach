package edu.pk.hach.protocol.model

import edu.pk.hach.protocol._
import edu.pk.hach.protocol.SessionStatus.SessionStatusType
import spray.http._
import spray.http.HttpMethods.{GET, PUT}
import spray.http.Uri._
import spray.http.StatusCodes._
import spray.http.HttpRequest
import spray.http.ChunkedRequestStart
import scala.Some
import spray.http.HttpResponse
import edu.pk.hach.protocol.meta.Server

trait StreamRequestFactory extends RequestFactory {
    val Url = "/stream"
    def uriWithSession[T](uri: Uri, action: (Option[String]) => Option[T]) = uri match {
        case PathUri(s, _*) => action(Some(s))
        case PathUri() => action(None)
        case _ => None
    }
}

case class SendStreamRequest(session: String) {

    import SendStreamRequestFactory._

    def toHttpRequest: ChunkedRequestStart =
        ChunkedRequestStart(HttpRequest(
            method = Method,
            uri = / withPath Path(Url) / session)
        )
}

object SendStreamRequestFactory extends StreamRequestFactory {
    val Method = PUT

    def unapply(request: HttpRequestPart): Option[Option[String]] = request match {
        case ChunkedRequestStart(HttpRequest(Method, uri, _, _, _)) => uriWithSession(uri, x => Some(x))
        case _ => None
    }
}

case class SendStreamResponse(sessionStatus: SessionStatusType) extends SuccessResponse {
    override protected val content = Map("status" -> sessionStatus.toString)
}

case class ReceiveStreamRequest(session: String, peer: String) extends Request {

    import ReceiveStreamRequestFactory._

    override def toHttpRequest: HttpRequest =
        HttpRequest(Method, / withPath Path(Url) / session / peer)
}

object ReceiveStreamRequestFactory extends StreamRequestFactory {
    val Method = GET

    def unapply(request: HttpRequest): Option[(Option[String], Option[String])] = request match {
        case HttpRequest(Method, uri, _, _, _) =>  uri match {
            case PathUri(s, "/", p, _*) => Some(Some(s), Some(p))
            case PathUri(s, _*) => Some(Some(s), None)
            case PathUri() => Some(None, None)
            case _ => None
        }
        case _ => None
    }
}

case class ReceiveStreamResponse() extends SuccessResponse

case class RedirectToNodeResponse(server: Server) extends BasicResponse {
    override val status: StatusCode = Found
    override protected val headers: List[HttpHeader] = {

        List(HttpHeaders.Location(Uri.from(host = server.host, port = server.port, scheme = "hach")))
    }
}

object RedirectToNodeResponseFactory {
    def unapply(response: HttpResponse): Option[Server] = response match {
        case HttpResponse(Found, _, headers, _) => headers.find(_.is("location")) match {
            case Some(HttpHeader(name, value)) =>
                val authority = Uri(value).authority
                Some(Server(authority.host.address, authority.port))
            case _ => None
        }
        case _ => None
    }
}