package edu.pk.hach.protocol

import spray.http.{Uri, HttpMethod}
import spray.http.Uri.Path

trait RequestFactory {
    val Method: HttpMethod
    val Url: String

    object PathUri {
        def unapplySeq(uri: Uri): Option[Seq[String]] = {
            val urlPath = Path(Url)
            val uriPath = uri.path
            if (uriPath.startsWith(urlPath))
                Some(pathToSeq(0, urlPath.length + 1, uriPath))
            else
                None
        }

        def pathToSeq(i: Int, n: Int, path: Path): List[String] = {
            if (path.length == 0) List[String]()
            else if (i < n) pathToSeq(i + 1, n, path.tail)
            else path.head.toString :: pathToSeq(i + 1, n, path.tail)
        }
    }
}
