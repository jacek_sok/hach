package edu.pk.hach.protocol.model

import edu.pk.hach.protocol.{SessionStatus, UnitSpec}
import edu.pk.hach.protocol.EntityMarshaller._
import spray.http.StatusCodes

class StreamSpec extends UnitSpec {
    "SendStreamRequestFactory" should "match request and parameters" in {
        import SendStreamRequestFactory._

        val request = SendStreamRequest("123").toHttpRequest

        val result = unapply(request)

        result should not be None
    }

    "SendStreamResponse" should "contain status" in {
        val response = SendStreamResponse(SessionStatus.Opened).toHttpResponse

        response.status should be(StatusCodes.OK)
        unmarshall(response.entity) should contain("status" -> "Opened")
    }

    "ReceiveStreamRequestFactory" should "match request and parameter" in {
        import ReceiveStreamRequestFactory._
        val request = ReceiveStreamRequest("123", "A").toHttpRequest

        val result = unapply(request)

        result should not be None
        result.get._1 should be(Some("123"))
        result.get._2 should be(Some("A"))
    }
}
