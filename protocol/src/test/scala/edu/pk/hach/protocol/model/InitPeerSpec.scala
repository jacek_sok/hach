package edu.pk.hach.protocol.model

import edu.pk.hach.protocol.UnitSpec
import spray.http.{HttpHeaders, HttpRequest, StatusCodes}
import edu.pk.hach.protocol.EntityMarshaller._
import spray.http.Uri._

class InitPeerSpec extends UnitSpec {
    def addRemoteAddressHeader(rq: HttpRequest) =
        rq.copy(headers = HttpHeaders.`Remote-Address`("localhost") :: rq.headers)

    "InitPeerRequestFactory" should "match request and parameters" in {
        import InitPeerRequestFactory._
        val request = addRemoteAddressHeader(InitPeerRequest("123", "me").toHttpRequest)

        val result = unapply(request)

        result should not be None
        result.get._1 should equal(Some("123"))
        result.get._2 should equal(Some("me"))
        result.get._3 should not be empty
    }
    it should "match request without parameters" in {
        import InitPeerRequestFactory._
        val request = addRemoteAddressHeader(HttpRequest(Method, /.withPath(Path(Url))))

        val result = unapply(request)

        result should not be None
        result.get._1 should be(None)
        result.get._2 should be(None)
        result.get._3 should not be empty
    }


    "InitPeerRequest" should "contain session id" in {
        val response = InitPeerResponse("123", "321").toHttpResponse

        response.status should be(StatusCodes.OK)
        unmarshall(response.entity) should (contain("session" -> "123") and contain("id" -> "321"))
    }
}
