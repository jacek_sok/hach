package edu.pk.hach.protocol.model

import spray.http._
import spray.http.Uri._
import edu.pk.hach.protocol.EntityMarshaller._
import spray.http.HttpRequest
import scala.Some
import edu.pk.hach.protocol.UnitSpec

class CloseSpec extends UnitSpec {

    import CloseRequestFactory._

    "CloseRequestFactory" should "match request and extract parameter" in {
        val request = CloseRequest("session").toHttpRequest

        val result = unapply(request)

        result.get should be(Some("session"))
    }

    it should "match request without parameter" in {
        val request = HttpRequest(Method, /.withPath(Path(Url)))

        val result = unapply(request)

        result should be(Some(None))
    }

    "CloseResponse" should "should be empty" in {
        val response = CloseResponse.toHttpResponse

        response.status should be(StatusCodes.OK)
        unmarshall(response.entity).filter(_._1 != "timestamp") should be(empty)
    }
}
