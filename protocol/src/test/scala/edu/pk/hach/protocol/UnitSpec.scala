package edu.pk.hach.protocol

import org.scalatest._

abstract class UnitSpec extends FlatSpec with Inside with Matchers with Inspectors with OptionValues
