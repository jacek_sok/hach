package edu.pk.hach.protocol.model

import edu.pk.hach.protocol.UnitSpec
import spray.http.StatusCodes

class ErrorsSpec extends UnitSpec {
    "ErrorResponse" should "contain status code, details, request and stack trace" in {
        val request = InitRequest(multi = true)
        val status = StatusCodes.FailedDependency
        val response = ErrorResponse("Reason", request, status, Thread.currentThread().getStackTrace).toHttpResponse

        response.status should be(status)
        response.entity.asString should include("Reason: Reason\n")
        response.entity.asString should include regex ("Request: .*%s.*\n" format request.multi)
        response.entity.asString should include regex "Debug:\\s*\n.+"
    }
    it should "contain status code, details and request (without stack trace)" in {
        val request = InitRequest(multi = false)
        val response = ErrorResponse("Reason", request).toHttpResponse

        response.status should be(StatusCodes.InternalServerError)
        response.entity.asString should include("Reason: Reason\n")
        response.entity.asString should include regex ("Request: .*%s.*\n" format request.multi)
        response.entity.asString should include regex "Debug: n/a\n"
    }

    "InvalidSession" should "contain status code and session" in {
        val response = InvalidSession.toHttpResponse

        response.status should be(StatusCodes.BadRequest)
    }

    "StreamNotReady" should "contain status code and session" in {
        val response = StreamNotReady.toHttpResponse

        response.status should be(StatusCodes.Accepted)
    }
}
