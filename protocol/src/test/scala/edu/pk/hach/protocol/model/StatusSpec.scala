package edu.pk.hach.protocol.model

import spray.http.{StatusCodes, HttpRequest}
import spray.http.Uri.{Path, /}
import edu.pk.hach.protocol.{UnitSpec, SessionStatus}
import edu.pk.hach.protocol.EntityMarshaller._

class StatusSpec extends UnitSpec {

    import StatusRequestFactory._

    "StatusRequestFactory" should "match request and extract parameters" in {
        val request = StatusRequest("v2").toHttpRequest

        val result = unapply(request)

        result should not be None
        result.get should equal(Some("v2"))
    }
    it should "match request without parameter" in {
        val request = HttpRequest(Method, /.withPath(Path(Url)))

        val result = unapply(request)

        result should not be None
        result should equal(Some(None))
    }

    "StatusResponse" should "contain status code, peers and server string" in {
        val peers = Set(PeerInfo("1", "me", "x1"), PeerInfo("2", "you", "x2"))
        val response = StatusResponse(SessionStatus.Opened, peers, "Unit Test").toHttpResponse

        response.status should be(StatusCodes.OK)
        val map = unmarshall(response.entity.asString)
        map should (have size 4
            and contain("status" -> "Opened")
            and contain("serverString" -> "Unit Test"))


        val peersData: List[Map[String, String]] = map > "peers"
        peersData should have size 2
        peersData should contain(Map("id" -> "1", "name" -> "me", "host" -> "x1"))
    }

    "StatusResponseFactory" should "create response object from json" in {
        val peers = Set(PeerInfo("1", "me", "x1"), PeerInfo("2", "you", "x2"))
        val statusResponse = StatusResponse(SessionStatus.Opened, peers, "Unit Test").toHttpResponse

        val status = StatusResponseFactory.unapply(statusResponse)

        status should not be None
        status.get._1 should be(SessionStatus.Opened)
        status.get._3 should be("Unit Test")
        val peersRs = status.get._2
        peersRs should have size 2
        peersRs should contain(peers.toList(0))
        peersRs should contain(peers.toList(1))
    }
}
