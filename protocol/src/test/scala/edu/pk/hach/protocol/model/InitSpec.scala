package edu.pk.hach.protocol.model

import edu.pk.hach.protocol.UnitSpec
import edu.pk.hach.protocol.EntityMarshaller._
import spray.http.{StatusCodes, HttpRequest}
import spray.http.Uri._

class InitSpec extends UnitSpec {
    import InitRequestFactory._

    "InitRequestFactory" should "match request and extract parameter" in {
        val request = InitRequest(multi = true).toHttpRequest

        val result = unapply(request)

        result should be(Some(true))
    }

    it should "match request without parameter" in {
        val request = HttpRequest(Method, /.withPath(Path(Url)))

        val result = unapply(request)

        result should not be None
        result should be(Some(false))
    }

    "InitResponse" should "contain status code and session id" in {
        val response = InitResponse("1234", "4321").toHttpResponse

        response.status should be(StatusCodes.OK)
        unmarshall(response.entity) should contain("session" -> "1234")
        unmarshall(response.entity) should contain("exchange" -> "4321")
    }
}
